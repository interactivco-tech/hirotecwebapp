<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCustomerRequestAPIRequest;
use App\Http\Requests\API\UpdateCustomerRequestAPIRequest;
use App\Models\CustomerRequest;
use App\Repositories\CustomerRequestRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class CustomerRequestController
 * @package App\Http\Controllers\API
 */

class CustomerRequestAPIController extends AppBaseController
{
    /** @var  CustomerRequestRepository */
    private $customerRequestRepository;

    public function __construct(CustomerRequestRepository $customerRequestRepo)
    {
        $this->customerRequestRepository = $customerRequestRepo;
    }

    /**
     * Display a listing of the CustomerRequest.
     * GET|HEAD /customerRequests
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $customerRequests = $this->customerRequestRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($customerRequests->toArray(), 'Customer Requests retrieved successfully');
    }

    /**
     * Store a newly created CustomerRequest in storage.
     * POST /customerRequests
     *
     * @param CreateCustomerRequestAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateCustomerRequestAPIRequest $request)
    {
        $input = $request->all();

        $customerRequest = $this->customerRequestRepository->create($input);

        return $this->sendResponse($customerRequest->toArray(), 'Customer Request saved successfully');
    }

    /**
     * Display the specified CustomerRequest.
     * GET|HEAD /customerRequests/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var CustomerRequest $customerRequest */
        $customerRequest = $this->customerRequestRepository->find($id);

        if (empty($customerRequest)) {
            return $this->sendError('Customer Request not found');
        }

        return $this->sendResponse($customerRequest->toArray(), 'Customer Request retrieved successfully');
    }

    /**
     * Update the specified CustomerRequest in storage.
     * PUT/PATCH /customerRequests/{id}
     *
     * @param int $id
     * @param UpdateCustomerRequestAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCustomerRequestAPIRequest $request)
    {
        $input = $request->all();

        /** @var CustomerRequest $customerRequest */
        $customerRequest = $this->customerRequestRepository->find($id);

        if (empty($customerRequest)) {
            return $this->sendError('Customer Request not found');
        }

        $customerRequest = $this->customerRequestRepository->update($input, $id);

        return $this->sendResponse($customerRequest->toArray(), 'CustomerRequest updated successfully');
    }

    /**
     * Remove the specified CustomerRequest from storage.
     * DELETE /customerRequests/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var CustomerRequest $customerRequest */
        $customerRequest = $this->customerRequestRepository->find($id);

        if (empty($customerRequest)) {
            return $this->sendError('Customer Request not found');
        }

        $customerRequest->delete();

        return $this->sendSuccess('Customer Request deleted successfully');
    }
}
