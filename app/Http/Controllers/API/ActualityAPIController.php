<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateActualityAPIRequest;
use App\Http\Requests\API\UpdateActualityAPIRequest;
use App\Models\Actuality;
use App\Repositories\ActualityRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class ActualityController
 * @package App\Http\Controllers\API
 */

class ActualityAPIController extends AppBaseController
{
    /** @var  ActualityRepository */
    private $actualityRepository;

    public function __construct(ActualityRepository $actualityRepo)
    {
        $this->actualityRepository = $actualityRepo;
    }

    /**
     * Display a listing of the Actuality.
     * GET|HEAD /actualities
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $actualities = $this->actualityRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($actualities->toArray(), 'Actualities retrieved successfully');
    }

    /**
     * Store a newly created Actuality in storage.
     * POST /actualities
     *
     * @param CreateActualityAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateActualityAPIRequest $request)
    {
        $input = $request->all();

        $actuality = $this->actualityRepository->create($input);

        return $this->sendResponse($actuality->toArray(), 'Actuality saved successfully');
    }

    /**
     * Display the specified Actuality.
     * GET|HEAD /actualities/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Actuality $actuality */
        $actuality = $this->actualityRepository->find($id);

        if (empty($actuality)) {
            return $this->sendError('Actuality not found');
        }

        return $this->sendResponse($actuality->toArray(), 'Actuality retrieved successfully');
    }

    /**
     * Update the specified Actuality in storage.
     * PUT/PATCH /actualities/{id}
     *
     * @param int $id
     * @param UpdateActualityAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateActualityAPIRequest $request)
    {
        $input = $request->all();

        /** @var Actuality $actuality */
        $actuality = $this->actualityRepository->find($id);

        if (empty($actuality)) {
            return $this->sendError('Actuality not found');
        }

        $actuality = $this->actualityRepository->update($input, $id);

        return $this->sendResponse($actuality->toArray(), 'Actuality updated successfully');
    }

    /**
     * Remove the specified Actuality from storage.
     * DELETE /actualities/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Actuality $actuality */
        $actuality = $this->actualityRepository->find($id);

        if (empty($actuality)) {
            return $this->sendError('Actuality not found');
        }

        $actuality->delete();

        return $this->sendSuccess('Actuality deleted successfully');
    }
}
