<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateTradeAPIRequest;
use App\Http\Requests\API\UpdateTradeAPIRequest;
use App\Models\Trade;
use App\Repositories\TradeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class TradeController
 * @package App\Http\Controllers\API
 */

class TradeAPIController extends AppBaseController
{
    /** @var  TradeRepository */
    private $tradeRepository;

    public function __construct(TradeRepository $tradeRepo)
    {
        $this->tradeRepository = $tradeRepo;
    }

    /**
     * Display a listing of the Trade.
     * GET|HEAD /trades
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $trades = $this->tradeRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($trades->toArray(), 'Trades retrieved successfully');
    }

    /**
     * Store a newly created Trade in storage.
     * POST /trades
     *
     * @param CreateTradeAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateTradeAPIRequest $request)
    {
        $input = $request->all();

        $trade = $this->tradeRepository->create($input);

        return $this->sendResponse($trade->toArray(), 'Trade saved successfully');
    }

    /**
     * Display the specified Trade.
     * GET|HEAD /trades/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Trade $trade */
        $trade = $this->tradeRepository->find($id);

        if (empty($trade)) {
            return $this->sendError('Trade not found');
        }

        return $this->sendResponse($trade->toArray(), 'Trade retrieved successfully');
    }

    /**
     * Update the specified Trade in storage.
     * PUT/PATCH /trades/{id}
     *
     * @param int $id
     * @param UpdateTradeAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTradeAPIRequest $request)
    {
        $input = $request->all();

        /** @var Trade $trade */
        $trade = $this->tradeRepository->find($id);

        if (empty($trade)) {
            return $this->sendError('Trade not found');
        }

        $trade = $this->tradeRepository->update($input, $id);

        return $this->sendResponse($trade->toArray(), 'Trade updated successfully');
    }

    /**
     * Remove the specified Trade from storage.
     * DELETE /trades/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Trade $trade */
        $trade = $this->tradeRepository->find($id);

        if (empty($trade)) {
            return $this->sendError('Trade not found');
        }

        $trade->delete();

        return $this->sendSuccess('Trade deleted successfully');
    }
}
