<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateTestimonialAPIRequest;
use App\Http\Requests\API\UpdateTestimonialAPIRequest;
use App\Models\Testimonial;
use App\Repositories\TestimonialRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class TestimonialController
 * @package App\Http\Controllers\API
 */

class TestimonialAPIController extends AppBaseController
{
    /** @var  TestimonialRepository */
    private $testimonialRepository;

    public function __construct(TestimonialRepository $testimonialRepo)
    {
        $this->testimonialRepository = $testimonialRepo;
    }

    /**
     * Display a listing of the Testimonial.
     * GET|HEAD /testimonials
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $testimonials = $this->testimonialRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($testimonials->toArray(), 'Testimonials retrieved successfully');
    }

    /**
     * Store a newly created Testimonial in storage.
     * POST /testimonials
     *
     * @param CreateTestimonialAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateTestimonialAPIRequest $request)
    {
        $input = $request->all();

        $testimonial = $this->testimonialRepository->create($input);

        return $this->sendResponse($testimonial->toArray(), 'Testimonial saved successfully');
    }

    /**
     * Display the specified Testimonial.
     * GET|HEAD /testimonials/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Testimonial $testimonial */
        $testimonial = $this->testimonialRepository->find($id);

        if (empty($testimonial)) {
            return $this->sendError('Testimonial not found');
        }

        return $this->sendResponse($testimonial->toArray(), 'Testimonial retrieved successfully');
    }

    /**
     * Update the specified Testimonial in storage.
     * PUT/PATCH /testimonials/{id}
     *
     * @param int $id
     * @param UpdateTestimonialAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTestimonialAPIRequest $request)
    {
        $input = $request->all();

        /** @var Testimonial $testimonial */
        $testimonial = $this->testimonialRepository->find($id);

        if (empty($testimonial)) {
            return $this->sendError('Testimonial not found');
        }

        $testimonial = $this->testimonialRepository->update($input, $id);

        return $this->sendResponse($testimonial->toArray(), 'Testimonial updated successfully');
    }

    /**
     * Remove the specified Testimonial from storage.
     * DELETE /testimonials/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Testimonial $testimonial */
        $testimonial = $this->testimonialRepository->find($id);

        if (empty($testimonial)) {
            return $this->sendError('Testimonial not found');
        }

        $testimonial->delete();

        return $this->sendSuccess('Testimonial deleted successfully');
    }
}
