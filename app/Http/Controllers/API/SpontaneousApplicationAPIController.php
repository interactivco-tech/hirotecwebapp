<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSpontaneousApplicationAPIRequest;
use App\Http\Requests\API\UpdateSpontaneousApplicationAPIRequest;
use App\Models\SpontaneousApplication;
use App\Repositories\SpontaneousApplicationRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class SpontaneousApplicationController
 * @package App\Http\Controllers\API
 */

class SpontaneousApplicationAPIController extends AppBaseController
{
    /** @var  SpontaneousApplicationRepository */
    private $spontaneousApplicationRepository;

    public function __construct(SpontaneousApplicationRepository $spontaneousApplicationRepo)
    {
        $this->spontaneousApplicationRepository = $spontaneousApplicationRepo;
    }

    /**
     * Display a listing of the SpontaneousApplication.
     * GET|HEAD /spontaneousApplications
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $spontaneousApplications = $this->spontaneousApplicationRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($spontaneousApplications->toArray(), 'Spontaneous Applications retrieved successfully');
    }

    /**
     * Store a newly created SpontaneousApplication in storage.
     * POST /spontaneousApplications
     *
     * @param CreateSpontaneousApplicationAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateSpontaneousApplicationAPIRequest $request)
    {
        $input = $request->all();

        $spontaneousApplication = $this->spontaneousApplicationRepository->create($input);

        return $this->sendResponse($spontaneousApplication->toArray(), 'Spontaneous Application saved successfully');
    }

    /**
     * Display the specified SpontaneousApplication.
     * GET|HEAD /spontaneousApplications/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var SpontaneousApplication $spontaneousApplication */
        $spontaneousApplication = $this->spontaneousApplicationRepository->find($id);

        if (empty($spontaneousApplication)) {
            return $this->sendError('Spontaneous Application not found');
        }

        return $this->sendResponse($spontaneousApplication->toArray(), 'Spontaneous Application retrieved successfully');
    }

    /**
     * Update the specified SpontaneousApplication in storage.
     * PUT/PATCH /spontaneousApplications/{id}
     *
     * @param int $id
     * @param UpdateSpontaneousApplicationAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSpontaneousApplicationAPIRequest $request)
    {
        $input = $request->all();

        /** @var SpontaneousApplication $spontaneousApplication */
        $spontaneousApplication = $this->spontaneousApplicationRepository->find($id);

        if (empty($spontaneousApplication)) {
            return $this->sendError('Spontaneous Application not found');
        }

        $spontaneousApplication = $this->spontaneousApplicationRepository->update($input, $id);

        return $this->sendResponse($spontaneousApplication->toArray(), 'SpontaneousApplication updated successfully');
    }

    /**
     * Remove the specified SpontaneousApplication from storage.
     * DELETE /spontaneousApplications/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var SpontaneousApplication $spontaneousApplication */
        $spontaneousApplication = $this->spontaneousApplicationRepository->find($id);

        if (empty($spontaneousApplication)) {
            return $this->sendError('Spontaneous Application not found');
        }

        $spontaneousApplication->delete();

        return $this->sendSuccess('Spontaneous Application deleted successfully');
    }
}
