<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSpontaneousApplicationRequest;
use App\Http\Requests\UpdateSpontaneousApplicationRequest;
use App\Repositories\SpontaneousApplicationRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class SpontaneousApplicationController extends AppBaseController
{
    /** @var  SpontaneousApplicationRepository */
    private $spontaneousApplicationRepository;

    public function __construct(SpontaneousApplicationRepository $spontaneousApplicationRepo)
    {
        $this->spontaneousApplicationRepository = $spontaneousApplicationRepo;
    }

    /**
     * Display a listing of the SpontaneousApplication.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $spontaneousApplications = $this->spontaneousApplicationRepository->all();

        return view('spontaneous_applications.index')
            ->with('spontaneousApplications', $spontaneousApplications);
    }

    /**
     * Show the form for creating a new SpontaneousApplication.
     *
     * @return Response
     */
    public function create()
    {
        return view('spontaneous_applications.create');
    }

    /**
     * Store a newly created SpontaneousApplication in storage.
     *
     * @param CreateSpontaneousApplicationRequest $request
     *
     * @return Response
     */
    public function store(CreateSpontaneousApplicationRequest $request)
    {
        $input = $request->all();

        $spontaneousApplication = $this->spontaneousApplicationRepository->create($input);

        Flash::success('Spontaneous Application saved successfully.');

        return redirect(route('spontaneousApplications.index'));
    }

    /**
     * Display the specified SpontaneousApplication.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $spontaneousApplication = $this->spontaneousApplicationRepository->find($id);

        if (empty($spontaneousApplication)) {
            Flash::error('Spontaneous Application not found');

            return redirect(route('spontaneousApplications.index'));
        }

        return view('spontaneous_applications.show')->with('spontaneousApplication', $spontaneousApplication);
    }

    /**
     * Show the form for editing the specified SpontaneousApplication.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $spontaneousApplication = $this->spontaneousApplicationRepository->find($id);

        if (empty($spontaneousApplication)) {
            Flash::error('Spontaneous Application not found');

            return redirect(route('spontaneousApplications.index'));
        }

        return view('spontaneous_applications.edit')->with('spontaneousApplication', $spontaneousApplication);
    }

    /**
     * Update the specified SpontaneousApplication in storage.
     *
     * @param int $id
     * @param UpdateSpontaneousApplicationRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSpontaneousApplicationRequest $request)
    {
        $spontaneousApplication = $this->spontaneousApplicationRepository->find($id);

        if (empty($spontaneousApplication)) {
            Flash::error('Spontaneous Application not found');

            return redirect(route('spontaneousApplications.index'));
        }

        $spontaneousApplication = $this->spontaneousApplicationRepository->update($request->all(), $id);

        Flash::success('Spontaneous Application updated successfully.');

        return redirect(route('spontaneousApplications.index'));
    }

    /**
     * Remove the specified SpontaneousApplication from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $spontaneousApplication = $this->spontaneousApplicationRepository->find($id);

        if (empty($spontaneousApplication)) {
            Flash::error('Spontaneous Application not found');

            return redirect(route('spontaneousApplications.index'));
        }

        $this->spontaneousApplicationRepository->delete($id);

        Flash::success('Spontaneous Application deleted successfully.');

        return redirect(route('spontaneousApplications.index'));
    }
}
