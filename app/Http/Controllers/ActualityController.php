<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateActualityRequest;
use App\Http\Requests\UpdateActualityRequest;
use App\Repositories\ActualityRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\Actuality;
use Illuminate\Http\Request;
use Flash;
use Response;

class ActualityController extends AppBaseController
{
    /** @var  ActualityRepository */
    private $actualityRepository;

    public function __construct(ActualityRepository $actualityRepo)
    {
        $this->actualityRepository = $actualityRepo;
    }

    /**
     * Display a listing of the Actuality.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        // $actualities = $this->actualityRepository->all();
        $actualities = Actuality::orderBy('id', 'desc')->paginate(10);

        return view('actualities.index')
            ->with('actualities', $actualities);
    }

    /**
     * Show the form for creating a new Actuality.
     *
     * @return Response
     */
    public function create()
    {
        return view('actualities.create');
    }

    /**
     * Store a newly created Actuality in storage.
     *
     * @param CreateActualityRequest $request
     *
     * @return Response
     */
    public function store(CreateActualityRequest $request)
    {
        $input = $request->all();

        if ($request->hasFile('image'))
        {
            $destination_path = 'public/images';
            $image = $request->file('image');
            $image_name = $image->getClientOriginalName();
            $path = $request->file('image')->storeAs($destination_path, $image_name);

            $input['image'] = $image_name;
        }


        if ($request->file('medias')) {

            $pictures = [];
            $image = $request->file('medias');
            if ($image) {
                for ($i = 0; $i < sizeof($image); $i++) {
                    ${'image' . ($i)} = $image[$i]->store('storage/images', ['disk' => 'public']);
                    ${'picture' . ($i)} = ${'image' . ($i)} ?? null;
                    array_push($pictures, ${'picture' . ($i)});
                }
                // dd($pictures);
            }

        } else {
            $pictures = null;
        }

        $projectImageLink = json_encode($pictures);

        $input["medias"] = $projectImageLink;

        $actuality = $this->actualityRepository->create($input);

        Flash::success('Actuality saved successfully.');

        return redirect(route('actualities.index'));
    }

    /**
     * Display the specified Actuality.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $actuality = $this->actualityRepository->find($id);

        if (empty($actuality)) {
            Flash::error('Actuality not found');

            return redirect(route('actualities.index'));
        }

        return view('actualities.show')->with('actuality', $actuality);
    }

    /**
     * Show the form for editing the specified Actuality.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $actuality = $this->actualityRepository->find($id);

        if (empty($actuality)) {
            Flash::error('Actuality not found');

            return redirect(route('actualities.index'));
        }

        return view('actualities.edit')->with('actuality', $actuality);
    }

    /**
     * Update the specified Actuality in storage.
     *
     * @param int $id
     * @param UpdateActualityRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateActualityRequest $request)
    {
        $actuality = $this->actualityRepository->find($id);

        if (empty($actuality)) {
            Flash::error('Actuality not found');

            return redirect(route('actualities.index'));
        }

        $input = $request->all();

        if ($request->hasFile('image'))
        {
            $destination_path = 'public/images';
            $image = $request->file('image');
            $image_name = $image->getClientOriginalName();
            $path = $request->file('image')->storeAs($destination_path, $image_name);

            $input['image'] = $image_name;
        }


        if ($request->file('medias')) {

            $pictures = [];
            $image = $request->file('medias');
            if ($image) {
                for ($i = 0; $i < sizeof($image); $i++) {
                    ${'image' . ($i)} = $image[$i]->store('public/images', ['disk' => 'public']);
                    ${'picture' . ($i)} = ${'image' . ($i)} ?? null;
                    array_push($pictures, ${'picture' . ($i)});
                }
                // dd($pictures);
            }

        } else {
            $pictures = null;
        }

        $projectImageLink = json_encode($pictures);

        $input["medias"] = $projectImageLink;

        $input['actuality_date'] = $actuality->actuality_date;

        $actuality = $this->actualityRepository->update($request->all(), $id);

        Flash::success('Actuality updated successfully.');

        return redirect(route('actualities.index'));
    }

    /**
     * Remove the specified Actuality from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $actuality = $this->actualityRepository->find($id);

        if (empty($actuality)) {
            Flash::error('Actuality not found');

            return redirect(route('actualities.index'));
        }

        $this->actualityRepository->delete($id);

        Flash::success('Actuality deleted successfully.');

        return redirect(route('actualities.index'));
    }
}
