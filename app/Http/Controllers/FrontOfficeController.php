<?php

namespace App\Http\Controllers;

use App\Models\Actuality;
use App\Models\CategoryProject;
use App\Models\Project;
use App\Models\Slide;
use App\Models\Team;
use App\Models\Testimonial;
use Illuminate\Http\Request;

class FrontOfficeController extends Controller
{
    public function HomePage()
    {
        $teams = Team::take(5)->get();

        $testimonials = Testimonial::take(3)->get();

        $slides = Slide::take(3)->get();

        $projects = Project::all();

        $categoryProjects = CategoryProject::with('projects')->get();

        return view()->exists('customerFrontOffice.index') ? view('customerFrontOffice.index', compact('testimonials', 'teams', 'slides', 'projects', 'categoryProjects')) : '';
    }

    public function Contact()
    {
        return view('customerFrontOffice.contact');
    }

    public function About()
    {
        $teams = Team::all();

        return view('customerFrontOffice.about', compact(('teams')));
    }

    public function knowNow()
    {
        return view('customerFrontOffice.know_now');
    }

    public function IndexActualities()
    {
        $actualities = Actuality::paginate(6);

        return view('customerFrontOffice.actuality.index', compact('actualities'));
    }

    public function showActualities($slug)
    {
        $actualitie = Actuality::where('slug', $slug)->first();

        $actualities = Actuality::take(2)->get();

        return view('customerFrontOffice.actuality.show', compact('actualities', 'actualitie'));
    }

    public function IndexProjects()
    {

        $categoryProjects = CategoryProject::with('projects')->get();

        return view()->exists('customerFrontOffice.realisation.index') ? view('customerFrontOffice.realisation.index', compact('categoryProjects')) : '';
    }

    public function showProject($slug)
    {
        $project = Project::where('slug', $slug)->first();

        $projects = Project::take(2)->get();

        return view('customerFrontOffice.realisation.show', compact('project', 'projects'));
    }

}

