<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCustomerRequestRequest;
use App\Http\Requests\UpdateCustomerRequestRequest;
use App\Repositories\CustomerRequestRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\CustomerRequest;
use Illuminate\Http\Request;
use Flash;
use Response;

class CustomerRequestController extends AppBaseController
{
    /** @var  CustomerRequestRepository */
    private $customerRequestRepository;

    public function __construct(CustomerRequestRepository $customerRequestRepo)
    {
        $this->customerRequestRepository = $customerRequestRepo;
    }

    /**
     * Display a listing of the CustomerRequest.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $customerRequests = $this->customerRequestRepository->all();

        return view('customer_requests.index')
            ->with('customerRequests', $customerRequests);
    }

    /**
     * Show the form for creating a new CustomerRequest.
     *
     * @return Response
     */
    public function create()
    {
        return view('customer_requests.create');
    }

    /**
     * Store a newly created CustomerRequest in storage.
     *
     * @param CreateCustomerRequestRequest $request
     *
     * @return Response
     */
    public function store(CreateCustomerRequestRequest $request)
    {
        $input = $request->all();

        $customerRequest = $this->customerRequestRepository->create($input);

        Flash::success('Customer Request saved successfully.');

        return redirect(route('customerRequests.index'));
    }

    /**
     * Display the specified CustomerRequest.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $customerRequest = $this->customerRequestRepository->find($id);

        if (empty($customerRequest)) {
            Flash::error('Customer Request not found');

            return redirect(route('customerRequests.index'));
        }

        return view('customer_requests.show')->with('customerRequest', $customerRequest);
    }

    /**
     * Show the form for editing the specified CustomerRequest.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $customerRequest = $this->customerRequestRepository->find($id);

        if (empty($customerRequest)) {
            Flash::error('Customer Request not found');

            return redirect(route('customerRequests.index'));
        }

        return view('customer_requests.edit')->with('customerRequest', $customerRequest);
    }

    /**
     * Update the specified CustomerRequest in storage.
     *
     * @param int $id
     * @param UpdateCustomerRequestRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCustomerRequestRequest $request)
    {
        $customerRequest = $this->customerRequestRepository->find($id);

        if (empty($customerRequest)) {
            Flash::error('Customer Request not found');

            return redirect(route('customerRequests.index'));
        }

        $customerRequest = $this->customerRequestRepository->update($request->all(), $id);

        Flash::success('Customer Request updated successfully.');

        return redirect(route('customerRequests.index'));
    }

    /**
     * Remove the specified CustomerRequest from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $customerRequest = $this->customerRequestRepository->find($id);

        if (empty($customerRequest)) {
            Flash::error('Customer Request not found');

            return redirect(route('customerRequests.index'));
        }

        $this->customerRequestRepository->delete($id);

        Flash::success('Customer Request deleted successfully.');

        return redirect(route('customerRequests.index'));
    }


    public function addCustomerRequest(CreateCustomerRequestRequest $request)
    {

        $data = $request->validate([
            'name' => 'required',
            'email' => 'required | email',
            'phone' => 'required',
            'subject' => 'required',
            'message' => 'required',
        ]);
        // dd($data);

        $customerRequest = new CustomerRequest();
        $customerRequest->name = $data['name'];
        $customerRequest->phone = $data['phone'];
        $customerRequest->email = $data['email'];
        $customerRequest->subject = $data['subject'];
        $customerRequest->message = $data['message'];

        $customerRequest->save();


        session()->flash('success', "Message envoyé enregistrée avec success");

        session(['name'=> $customerRequest->name]);

        return redirect()->back();
    }
}
