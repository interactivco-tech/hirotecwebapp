<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProjectRequest;
use App\Http\Requests\UpdateProjectRequest;
use App\Repositories\ProjectRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\CategoryProject;
use App\Models\Project;
use Illuminate\Http\Request;
use Flash;
use Response;

class ProjectController extends AppBaseController
{
    /** @var  ProjectRepository */
    private $projectRepository;

    public function __construct(ProjectRepository $projectRepo)
    {
        $this->projectRepository = $projectRepo;
    }

    /**
     * Display a listing of the Project.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        // $projects = $this->projectRepository->all();
        $projects = Project::orderBy('id', 'desc')->paginate(10);

        return view('projects.index')
            ->with('projects', $projects);
    }

    /**
     * Show the form for creating a new Project.
     *
     * @return Response
     */
    public function create()
    {
        $categoryProjects = CategoryProject::all();

        return view('projects.create', compact('categoryProjects'));
    }

    /**
     * Store a newly created Project in storage.
     *
     * @param CreateProjectRequest $request
     *
     * @return Response
     */
    public function store(CreateProjectRequest $request)
    {

        $input = $request->all();
        // dd($input);

        $categoryProject = CategoryProject::find($request->get("category_project_id"));
        // dd($categoryProject);


        if ($request->hasFile('image'))
        {
            $destination_path = 'public/images';
            $image = $request->file('image');
            $image_name = $image->getClientOriginalName();
            $path = $request->file('image')->storeAs($destination_path, $image_name);

            $input['image'] = $image_name;
        }

        if ($request->hasFile('picture_1'))
        {
            $destination_path = 'public/images';
            $image = $request->file('picture_1');
            $image_name = $image->getClientOriginalName();
            $path = $request->file('picture_1')->storeAs($destination_path, $image_name);

            $img = Project::make($image->path());
            $img->resize(110, 110, function ($const) {
                $const->aspectRatio();
            })->storeAs($destination_path, $image_name);

            $input['picture_1'] = $image_name;
        }

        if ($request->hasFile('picture_2'))
        {
            $destination_path = 'public/images';
            $image = $request->file('picture_2');
            $image_name = $image->getClientOriginalName();
            $path = $request->file('picture_2')->storeAs($destination_path, $image_name);

            $input['picture_2'] = $image_name;
        }

        if ($request->hasFile('picture_3'))
        {
            $destination_path = 'public/images';
            $image = $request->file('picture_3');
            $image_name = $image->getClientOriginalName();
            $path = $request->file('picture_3')->storeAs($destination_path, $image_name);

            $input['picture_3'] = $image_name;
        }


        $input["category_project_id"] = $categoryProject->id;

        $project = $this->projectRepository->create($input);

        Flash::success('Project saved successfully.');

        return redirect(route('projects.index'));
    }

    /**
     * Display the specified Project.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $project = $this->projectRepository->find($id);

        if (empty($project)) {
            Flash::error('Project not found');

            return redirect(route('projects.index'));
        }

        return view('projects.show')->with('project', $project);
    }

    /**
     * Show the form for editing the specified Project.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $project = $this->projectRepository->find($id);

        $categoryProjects = CategoryProject::all();

        if (empty($project)) {
            Flash::error('Project not found');

            return redirect(route('projects.index'));
        }

        return view('projects.edit')
            ->with('project', $project)
            ->with('categoryProjects', $categoryProjects);
    }

    /**
     * Update the specified Project in storage.
     *
     * @param int $id
     * @param UpdateProjectRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProjectRequest $request)
    {
        $project = $this->projectRepository->find($id);

        if (empty($project)) {
            Flash::error('Project not found');

            return redirect(route('projects.index'));
        }


        $input = $request->all();

        if ($request->hasFile('image'))
        {
            $destination_path = 'public/images';
            $image = $request->file('image');
            $image_name = $image->getClientOriginalName();
            $path = $request->file('image')->storeAs($destination_path, $image_name);

            $input['image'] = $image_name;
        }

        $input['start_of_work'] = $project->start_of_work;
        $input['end_of_work'] = $project->end_of_work;
        $input['description'] = $project->description;

        $project = $this->projectRepository->update($request->all(), $id);

        Flash::success('Project updated successfully.');

        return redirect(route('projects.index'));
    }

    /**
     * Remove the specified Project from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $project = $this->projectRepository->find($id);

        if (empty($project)) {
            Flash::error('Project not found');

            return redirect(route('projects.index'));
        }

        $this->projectRepository->delete($id);

        Flash::success('Project deleted successfully.');

        return redirect(route('projects.index'));
    }
}
