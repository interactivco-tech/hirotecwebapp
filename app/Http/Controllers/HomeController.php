<?php

namespace App\Http\Controllers;

use App\Models\Actuality;
use App\Models\CustomerRequest;
use App\Models\Newsletter;
use App\Models\Project;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $customerRequests = CustomerRequest::all();
        $countcustomerRequest = count($customerRequests);

        $actualities = Actuality::all();
        $countActuality = count($actualities);

        $projects = Project::all();
        $countProject = count($projects);

        $newsletters = Newsletter::all();
        $countnewletter = count($newsletters);

        return view('home', compact('countcustomerRequest', 'countActuality', 'countProject', 'countnewletter', 'customerRequests'));
    }
}
