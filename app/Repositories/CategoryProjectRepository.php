<?php

namespace App\Repositories;

use App\Models\CategoryProject;
use App\Repositories\BaseRepository;

/**
 * Class CategoryProjectRepository
 * @package App\Repositories
 * @version May 5, 2021, 4:29 pm UTC
*/

class CategoryProjectRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CategoryProject::class;
    }
}
