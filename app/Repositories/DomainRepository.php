<?php

namespace App\Repositories;

use App\Models\Domain;
use App\Repositories\BaseRepository;

/**
 * Class DomainRepository
 * @package App\Repositories
 * @version May 6, 2021, 3:43 pm UTC
*/

class DomainRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Domain::class;
    }
}
