<?php

namespace App\Repositories;

use App\Models\Actuality;
use App\Repositories\BaseRepository;

/**
 * Class ActualityRepository
 * @package App\Repositories
 * @version May 6, 2021, 4:20 pm UTC
*/

class ActualityRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Actuality::class;
    }
}
