<?php

namespace App\Repositories;

use App\Models\SpontaneousApplication;
use App\Repositories\BaseRepository;

/**
 * Class SpontaneousApplicationRepository
 * @package App\Repositories
 * @version May 5, 2021, 4:58 pm UTC
*/

class SpontaneousApplicationRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SpontaneousApplication::class;
    }
}
