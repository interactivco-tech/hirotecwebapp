<?php

namespace App\Repositories;

use App\Models\CustomerRequest;
use App\Repositories\BaseRepository;

/**
 * Class CustomerRequestRepository
 * @package App\Repositories
 * @version May 5, 2021, 4:00 pm UTC
*/

class CustomerRequestRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CustomerRequest::class;
    }
}
