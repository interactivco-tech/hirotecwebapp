<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class CustomerRequest
 * @package App\Models
 * @version May 5, 2021, 4:00 pm UTC
 *
 */
class CustomerRequest extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'customer_requests';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'phone',
        'email',
        'subject',
        'message'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'phone' => 'string',
        'email' => 'string',
        'subject' => 'string',
        'message' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


}
