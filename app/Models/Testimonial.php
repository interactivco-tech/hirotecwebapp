<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Testimonial
 * @package App\Models
 * @version May 5, 2021, 3:56 pm UTC
 *
 */
class Testimonial extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'testimonials';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'customer_name',
        'image',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'customer_name' => 'string',
        'image' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


}
