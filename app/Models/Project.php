<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Project
 * @package App\Models
 * @version May 5, 2021, 4:02 pm UTC
 *
 */
class Project extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'projects';


    protected $dates = ['deleted_at'];

    protected $appends = ['CategoryProject'];



    public $fillable = [
        'title',
        'slug',
        'project_owner',
        'project_manager',
        'description',
        'start_of_work',
        'end_of_work',
        'sub_description',
        'image',
        'picture_1',
        'picture_2',
        'picture_3',
        'location',
        'category_project_id',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'slug' => 'string',
        'category_project_id' => 'integer',
        'project_owner' => 'string',
        'project_manager' => 'string',
        'location' => 'string',
        'description' => 'string',
        'picture_1' => 'string',
        'picture_2' => 'string',
        'picture_3' => 'string',
        'image' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


    public function CategoryProject()
    {
        return $this->belongsTo(CategoryProject::class);
    }

    public function getCategoriesProjectAttribute()
    {

        $categoryProjects = CategoryProject::find($this->category_project_id);

        return $categoryProjects;
    }

}
