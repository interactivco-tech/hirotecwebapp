<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class SpontaneousApplication
 * @package App\Models
 * @version May 5, 2021, 4:58 pm UTC
 *
 */
class SpontaneousApplication extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'spontaneous_applications';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'phone',
        'email',
        'desired_position',
        'curriculum_vitae'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'phone' => 'string',
        'email' => 'string',
        'desired_position' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


}
