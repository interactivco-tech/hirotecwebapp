<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Trade
 * @package App\Models
 * @version May 6, 2021, 4:07 pm UTC
 *
 */
class Trade extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'trades';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'title',
        'slug',
        'subtitle',
        'description',
        'image',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'slug' => 'string',
        'subtitle' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


}
