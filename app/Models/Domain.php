<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Domain
 * @package App\Models
 * @version May 6, 2021, 3:43 pm UTC
 *
 */
class Domain extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'domains';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'image',
        'description',
        'medias'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'image' => 'string',
        'description' => 'string',
        'medias' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


}
