<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Actuality;

class ActualityApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_actuality()
    {
        $actuality = Actuality::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/actualities', $actuality
        );

        $this->assertApiResponse($actuality);
    }

    /**
     * @test
     */
    public function test_read_actuality()
    {
        $actuality = Actuality::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/actualities/'.$actuality->id
        );

        $this->assertApiResponse($actuality->toArray());
    }

    /**
     * @test
     */
    public function test_update_actuality()
    {
        $actuality = Actuality::factory()->create();
        $editedActuality = Actuality::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/actualities/'.$actuality->id,
            $editedActuality
        );

        $this->assertApiResponse($editedActuality);
    }

    /**
     * @test
     */
    public function test_delete_actuality()
    {
        $actuality = Actuality::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/actualities/'.$actuality->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/actualities/'.$actuality->id
        );

        $this->response->assertStatus(404);
    }
}
