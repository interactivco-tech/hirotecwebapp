<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\CustomerRequest;

class CustomerRequestApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_customer_request()
    {
        $customerRequest = CustomerRequest::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/customer_requests', $customerRequest
        );

        $this->assertApiResponse($customerRequest);
    }

    /**
     * @test
     */
    public function test_read_customer_request()
    {
        $customerRequest = CustomerRequest::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/customer_requests/'.$customerRequest->id
        );

        $this->assertApiResponse($customerRequest->toArray());
    }

    /**
     * @test
     */
    public function test_update_customer_request()
    {
        $customerRequest = CustomerRequest::factory()->create();
        $editedCustomerRequest = CustomerRequest::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/customer_requests/'.$customerRequest->id,
            $editedCustomerRequest
        );

        $this->assertApiResponse($editedCustomerRequest);
    }

    /**
     * @test
     */
    public function test_delete_customer_request()
    {
        $customerRequest = CustomerRequest::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/customer_requests/'.$customerRequest->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/customer_requests/'.$customerRequest->id
        );

        $this->response->assertStatus(404);
    }
}
