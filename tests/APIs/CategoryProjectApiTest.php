<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\CategoryProject;

class CategoryProjectApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_category_project()
    {
        $categoryProject = CategoryProject::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/category_projects', $categoryProject
        );

        $this->assertApiResponse($categoryProject);
    }

    /**
     * @test
     */
    public function test_read_category_project()
    {
        $categoryProject = CategoryProject::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/category_projects/'.$categoryProject->id
        );

        $this->assertApiResponse($categoryProject->toArray());
    }

    /**
     * @test
     */
    public function test_update_category_project()
    {
        $categoryProject = CategoryProject::factory()->create();
        $editedCategoryProject = CategoryProject::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/category_projects/'.$categoryProject->id,
            $editedCategoryProject
        );

        $this->assertApiResponse($editedCategoryProject);
    }

    /**
     * @test
     */
    public function test_delete_category_project()
    {
        $categoryProject = CategoryProject::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/category_projects/'.$categoryProject->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/category_projects/'.$categoryProject->id
        );

        $this->response->assertStatus(404);
    }
}
