<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\SpontaneousApplication;

class SpontaneousApplicationApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_spontaneous_application()
    {
        $spontaneousApplication = SpontaneousApplication::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/spontaneous_applications', $spontaneousApplication
        );

        $this->assertApiResponse($spontaneousApplication);
    }

    /**
     * @test
     */
    public function test_read_spontaneous_application()
    {
        $spontaneousApplication = SpontaneousApplication::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/spontaneous_applications/'.$spontaneousApplication->id
        );

        $this->assertApiResponse($spontaneousApplication->toArray());
    }

    /**
     * @test
     */
    public function test_update_spontaneous_application()
    {
        $spontaneousApplication = SpontaneousApplication::factory()->create();
        $editedSpontaneousApplication = SpontaneousApplication::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/spontaneous_applications/'.$spontaneousApplication->id,
            $editedSpontaneousApplication
        );

        $this->assertApiResponse($editedSpontaneousApplication);
    }

    /**
     * @test
     */
    public function test_delete_spontaneous_application()
    {
        $spontaneousApplication = SpontaneousApplication::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/spontaneous_applications/'.$spontaneousApplication->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/spontaneous_applications/'.$spontaneousApplication->id
        );

        $this->response->assertStatus(404);
    }
}
