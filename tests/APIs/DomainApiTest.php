<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Domain;

class DomainApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_domain()
    {
        $domain = Domain::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/domains', $domain
        );

        $this->assertApiResponse($domain);
    }

    /**
     * @test
     */
    public function test_read_domain()
    {
        $domain = Domain::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/domains/'.$domain->id
        );

        $this->assertApiResponse($domain->toArray());
    }

    /**
     * @test
     */
    public function test_update_domain()
    {
        $domain = Domain::factory()->create();
        $editedDomain = Domain::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/domains/'.$domain->id,
            $editedDomain
        );

        $this->assertApiResponse($editedDomain);
    }

    /**
     * @test
     */
    public function test_delete_domain()
    {
        $domain = Domain::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/domains/'.$domain->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/domains/'.$domain->id
        );

        $this->response->assertStatus(404);
    }
}
