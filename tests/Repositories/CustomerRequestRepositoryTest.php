<?php namespace Tests\Repositories;

use App\Models\CustomerRequest;
use App\Repositories\CustomerRequestRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class CustomerRequestRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var CustomerRequestRepository
     */
    protected $customerRequestRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->customerRequestRepo = \App::make(CustomerRequestRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_customer_request()
    {
        $customerRequest = CustomerRequest::factory()->make()->toArray();

        $createdCustomerRequest = $this->customerRequestRepo->create($customerRequest);

        $createdCustomerRequest = $createdCustomerRequest->toArray();
        $this->assertArrayHasKey('id', $createdCustomerRequest);
        $this->assertNotNull($createdCustomerRequest['id'], 'Created CustomerRequest must have id specified');
        $this->assertNotNull(CustomerRequest::find($createdCustomerRequest['id']), 'CustomerRequest with given id must be in DB');
        $this->assertModelData($customerRequest, $createdCustomerRequest);
    }

    /**
     * @test read
     */
    public function test_read_customer_request()
    {
        $customerRequest = CustomerRequest::factory()->create();

        $dbCustomerRequest = $this->customerRequestRepo->find($customerRequest->id);

        $dbCustomerRequest = $dbCustomerRequest->toArray();
        $this->assertModelData($customerRequest->toArray(), $dbCustomerRequest);
    }

    /**
     * @test update
     */
    public function test_update_customer_request()
    {
        $customerRequest = CustomerRequest::factory()->create();
        $fakeCustomerRequest = CustomerRequest::factory()->make()->toArray();

        $updatedCustomerRequest = $this->customerRequestRepo->update($fakeCustomerRequest, $customerRequest->id);

        $this->assertModelData($fakeCustomerRequest, $updatedCustomerRequest->toArray());
        $dbCustomerRequest = $this->customerRequestRepo->find($customerRequest->id);
        $this->assertModelData($fakeCustomerRequest, $dbCustomerRequest->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_customer_request()
    {
        $customerRequest = CustomerRequest::factory()->create();

        $resp = $this->customerRequestRepo->delete($customerRequest->id);

        $this->assertTrue($resp);
        $this->assertNull(CustomerRequest::find($customerRequest->id), 'CustomerRequest should not exist in DB');
    }
}
