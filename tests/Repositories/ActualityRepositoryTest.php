<?php namespace Tests\Repositories;

use App\Models\Actuality;
use App\Repositories\ActualityRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class ActualityRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var ActualityRepository
     */
    protected $actualityRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->actualityRepo = \App::make(ActualityRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_actuality()
    {
        $actuality = Actuality::factory()->make()->toArray();

        $createdActuality = $this->actualityRepo->create($actuality);

        $createdActuality = $createdActuality->toArray();
        $this->assertArrayHasKey('id', $createdActuality);
        $this->assertNotNull($createdActuality['id'], 'Created Actuality must have id specified');
        $this->assertNotNull(Actuality::find($createdActuality['id']), 'Actuality with given id must be in DB');
        $this->assertModelData($actuality, $createdActuality);
    }

    /**
     * @test read
     */
    public function test_read_actuality()
    {
        $actuality = Actuality::factory()->create();

        $dbActuality = $this->actualityRepo->find($actuality->id);

        $dbActuality = $dbActuality->toArray();
        $this->assertModelData($actuality->toArray(), $dbActuality);
    }

    /**
     * @test update
     */
    public function test_update_actuality()
    {
        $actuality = Actuality::factory()->create();
        $fakeActuality = Actuality::factory()->make()->toArray();

        $updatedActuality = $this->actualityRepo->update($fakeActuality, $actuality->id);

        $this->assertModelData($fakeActuality, $updatedActuality->toArray());
        $dbActuality = $this->actualityRepo->find($actuality->id);
        $this->assertModelData($fakeActuality, $dbActuality->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_actuality()
    {
        $actuality = Actuality::factory()->create();

        $resp = $this->actualityRepo->delete($actuality->id);

        $this->assertTrue($resp);
        $this->assertNull(Actuality::find($actuality->id), 'Actuality should not exist in DB');
    }
}
