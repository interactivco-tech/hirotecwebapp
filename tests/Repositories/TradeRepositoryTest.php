<?php namespace Tests\Repositories;

use App\Models\Trade;
use App\Repositories\TradeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class TradeRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var TradeRepository
     */
    protected $tradeRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->tradeRepo = \App::make(TradeRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_trade()
    {
        $trade = Trade::factory()->make()->toArray();

        $createdTrade = $this->tradeRepo->create($trade);

        $createdTrade = $createdTrade->toArray();
        $this->assertArrayHasKey('id', $createdTrade);
        $this->assertNotNull($createdTrade['id'], 'Created Trade must have id specified');
        $this->assertNotNull(Trade::find($createdTrade['id']), 'Trade with given id must be in DB');
        $this->assertModelData($trade, $createdTrade);
    }

    /**
     * @test read
     */
    public function test_read_trade()
    {
        $trade = Trade::factory()->create();

        $dbTrade = $this->tradeRepo->find($trade->id);

        $dbTrade = $dbTrade->toArray();
        $this->assertModelData($trade->toArray(), $dbTrade);
    }

    /**
     * @test update
     */
    public function test_update_trade()
    {
        $trade = Trade::factory()->create();
        $fakeTrade = Trade::factory()->make()->toArray();

        $updatedTrade = $this->tradeRepo->update($fakeTrade, $trade->id);

        $this->assertModelData($fakeTrade, $updatedTrade->toArray());
        $dbTrade = $this->tradeRepo->find($trade->id);
        $this->assertModelData($fakeTrade, $dbTrade->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_trade()
    {
        $trade = Trade::factory()->create();

        $resp = $this->tradeRepo->delete($trade->id);

        $this->assertTrue($resp);
        $this->assertNull(Trade::find($trade->id), 'Trade should not exist in DB');
    }
}
