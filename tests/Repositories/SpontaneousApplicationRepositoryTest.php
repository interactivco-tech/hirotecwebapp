<?php namespace Tests\Repositories;

use App\Models\SpontaneousApplication;
use App\Repositories\SpontaneousApplicationRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class SpontaneousApplicationRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var SpontaneousApplicationRepository
     */
    protected $spontaneousApplicationRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->spontaneousApplicationRepo = \App::make(SpontaneousApplicationRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_spontaneous_application()
    {
        $spontaneousApplication = SpontaneousApplication::factory()->make()->toArray();

        $createdSpontaneousApplication = $this->spontaneousApplicationRepo->create($spontaneousApplication);

        $createdSpontaneousApplication = $createdSpontaneousApplication->toArray();
        $this->assertArrayHasKey('id', $createdSpontaneousApplication);
        $this->assertNotNull($createdSpontaneousApplication['id'], 'Created SpontaneousApplication must have id specified');
        $this->assertNotNull(SpontaneousApplication::find($createdSpontaneousApplication['id']), 'SpontaneousApplication with given id must be in DB');
        $this->assertModelData($spontaneousApplication, $createdSpontaneousApplication);
    }

    /**
     * @test read
     */
    public function test_read_spontaneous_application()
    {
        $spontaneousApplication = SpontaneousApplication::factory()->create();

        $dbSpontaneousApplication = $this->spontaneousApplicationRepo->find($spontaneousApplication->id);

        $dbSpontaneousApplication = $dbSpontaneousApplication->toArray();
        $this->assertModelData($spontaneousApplication->toArray(), $dbSpontaneousApplication);
    }

    /**
     * @test update
     */
    public function test_update_spontaneous_application()
    {
        $spontaneousApplication = SpontaneousApplication::factory()->create();
        $fakeSpontaneousApplication = SpontaneousApplication::factory()->make()->toArray();

        $updatedSpontaneousApplication = $this->spontaneousApplicationRepo->update($fakeSpontaneousApplication, $spontaneousApplication->id);

        $this->assertModelData($fakeSpontaneousApplication, $updatedSpontaneousApplication->toArray());
        $dbSpontaneousApplication = $this->spontaneousApplicationRepo->find($spontaneousApplication->id);
        $this->assertModelData($fakeSpontaneousApplication, $dbSpontaneousApplication->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_spontaneous_application()
    {
        $spontaneousApplication = SpontaneousApplication::factory()->create();

        $resp = $this->spontaneousApplicationRepo->delete($spontaneousApplication->id);

        $this->assertTrue($resp);
        $this->assertNull(SpontaneousApplication::find($spontaneousApplication->id), 'SpontaneousApplication should not exist in DB');
    }
}
