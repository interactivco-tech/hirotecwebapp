<?php namespace Tests\Repositories;

use App\Models\Domain;
use App\Repositories\DomainRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class DomainRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var DomainRepository
     */
    protected $domainRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->domainRepo = \App::make(DomainRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_domain()
    {
        $domain = Domain::factory()->make()->toArray();

        $createdDomain = $this->domainRepo->create($domain);

        $createdDomain = $createdDomain->toArray();
        $this->assertArrayHasKey('id', $createdDomain);
        $this->assertNotNull($createdDomain['id'], 'Created Domain must have id specified');
        $this->assertNotNull(Domain::find($createdDomain['id']), 'Domain with given id must be in DB');
        $this->assertModelData($domain, $createdDomain);
    }

    /**
     * @test read
     */
    public function test_read_domain()
    {
        $domain = Domain::factory()->create();

        $dbDomain = $this->domainRepo->find($domain->id);

        $dbDomain = $dbDomain->toArray();
        $this->assertModelData($domain->toArray(), $dbDomain);
    }

    /**
     * @test update
     */
    public function test_update_domain()
    {
        $domain = Domain::factory()->create();
        $fakeDomain = Domain::factory()->make()->toArray();

        $updatedDomain = $this->domainRepo->update($fakeDomain, $domain->id);

        $this->assertModelData($fakeDomain, $updatedDomain->toArray());
        $dbDomain = $this->domainRepo->find($domain->id);
        $this->assertModelData($fakeDomain, $dbDomain->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_domain()
    {
        $domain = Domain::factory()->create();

        $resp = $this->domainRepo->delete($domain->id);

        $this->assertTrue($resp);
        $this->assertNull(Domain::find($domain->id), 'Domain should not exist in DB');
    }
}
