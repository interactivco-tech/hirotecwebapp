<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::resource('teams', App\Http\Controllers\API\TeamAPIController::class);

Route::resource('testimonials', App\Http\Controllers\API\TestimonialAPIController::class);

Route::resource('customer_requests', App\Http\Controllers\API\CustomerRequestAPIController::class);

Route::resource('projects', App\Http\Controllers\API\ProjectAPIController::class);

Route::resource('category_projects', App\Http\Controllers\API\CategoryProjectAPIController::class);

Route::resource('slides', App\Http\Controllers\API\SlideAPIController::class);

Route::resource('spontaneous_applications', App\Http\Controllers\API\SpontaneousApplicationAPIController::class);

Route::resource('newsletters', App\Http\Controllers\API\NewsletterAPIController::class);

Route::resource('domains', App\Http\Controllers\API\DomainAPIController::class);

Route::resource('trades', App\Http\Controllers\API\TradeAPIController::class);

Route::resource('actualities', App\Http\Controllers\API\ActualityAPIController::class);

Route::resource('users', App\Http\Controllers\API\UserAPIController::class);
