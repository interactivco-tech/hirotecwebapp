<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Artisan::call('storage:link');

// Route::get('/', function () {
//     return view('welcome');
// });


Route::get('/', [App\Http\Controllers\FrontOfficeController::class, 'HomePage']);

Route::get('/contact', [App\Http\Controllers\FrontOfficeController::class, "Contact"])->name('contact');

Route::get('/apropos', [App\Http\Controllers\FrontOfficeController::class, "About"])->name('apropos');

Route::get('/savoir-faire', [App\Http\Controllers\FrontOfficeController::class, "knowNow"])->name('savoir-faire');

Route::get('/actualites', [App\Http\Controllers\FrontOfficeController::class, "IndexActualities"])->name('actualites');

Route::get('/detailactualites/{slug}', [App\Http\Controllers\FrontOfficeController::class, "showActualities"]);

Route::get('/realisations', [App\Http\Controllers\FrontOfficeController::class, "IndexProjects"])->name('realisations');

Route::get('/detailrealisation/{slug}', [App\Http\Controllers\FrontOfficeController::class, "showProject"]);

Route::post('/footer', [App\Http\Controllers\NewsletterController::class, "addEmail"])->name('footer');

Route::post('/contacter', [App\Http\Controllers\CustomerRequestController::class, "addCustomerRequest"])->name('contacter');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::resource('teams', App\Http\Controllers\TeamController::class);

Route::resource('testimonials', App\Http\Controllers\TestimonialController::class);

Route::resource('customerRequests', App\Http\Controllers\CustomerRequestController::class);

Route::resource('projects', App\Http\Controllers\ProjectController::class);

Route::post('ckeditor/upload', [App\Http\Controllers\CKEditorController::class, "upload"])->name('upload');

Route::resource('categoryProjects', App\Http\Controllers\CategoryProjectController::class);

Route::resource('slides', App\Http\Controllers\SlideController::class);

Route::resource('spontaneousApplications', App\Http\Controllers\SpontaneousApplicationController::class);

Route::resource('newsletters', App\Http\Controllers\NewsletterController::class);

Route::resource('domains', App\Http\Controllers\DomainController::class);

Route::resource('trades', App\Http\Controllers\TradeController::class);

Route::resource('actualities', App\Http\Controllers\ActualityController::class);


Route::resource('users', App\Http\Controllers\UserController::class);
