<div class="card-body">
    <table class="table table-striped" id="table1">
        <thead>
            <tr>
                <th>Nom du Candidat</th>
                <th>Contact du Candidat</th>
                <th>Post Souhaité</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($spontaneousApplications as $spontaneousApplication)
                <tr>

                    <td>{{ $spontaneousApplication->name }}</td>
                    <td>{{ $spontaneousApplication->phone }}</td>
                    <td>{{ $spontaneousApplication->desired_position }}</td>
                    <td width="120">
                        {!! Form::open(['route' => ['spontaneousApplications.destroy', $spontaneousApplication->id], 'method' => 'delete']) !!}
                            <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                <button type="button" class="btn btn-primary"><a href="{{ route('spontaneousApplications.show', [$spontaneousApplication->id]) }}" class='badge badge-pill badge-primary'>
                                    Voir
                                </a></button>
                                {{-- <button type="button" class="btn btn-warning"><a href="{{ route('spontaneousApplications.edit', [$spontaneousApplication->id]) }}" class='badge badge-pill badge-warning'>
                                    Editer
                                </a></button> --}}
                                {!! Form::button('Supprimer', ['type' => 'submit', 'class' => 'btn btn-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                            </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
