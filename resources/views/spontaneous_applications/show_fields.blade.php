<div class="row">
    <!-- Name At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('name', 'Nom du Candidat:') !!}
            <p>{{ $spontaneousApplication->name }}</p>
        </div>
    </div>
    <!-- Phone At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('phone', 'Contact du Candidat:') !!}
            <p>{{ $spontaneousApplication->phone }}</p>
        </div>
    </div>
</div>

<div class="row">
    <!-- Email At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('email', 'Email du Candidat:') !!}
            <p>{{ $spontaneousApplication->email }}</p>
        </div>
    </div>
    <!-- Desired_position At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('desired_position', 'Post Souhaité:') !!}
            <p>{{ $spontaneousApplication->desired_position }}</p>
        </div>
    </div>
</div>

<div class="row">
    <!-- Curriculum_vitae At Field -->
    <div class="col-md-12 col-12">
        <div class="form-group">
            {!! Form::label('curriculum_vitae', 'Curriculum Vitae:') !!}
            <p>{{ $spontaneousApplication->curriculum_vitae }}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <!-- Created At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('created_at', 'Soumis à:') !!}
            <p>{{ $spontaneousApplication->created_at }}</p>
        </div>
    </div>
    <!-- Updated At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('updated_at', 'Modifié à:') !!}
            <p>{{ $spontaneousApplication->updated_at }}</p>
        </div>
    </div>
</div>
