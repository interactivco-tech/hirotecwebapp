@extends('BackOffice_layouts.master_index')

@section('title')
    Nos Candidatures Spontanées | {{ config('app.name') }}
@endsection

@section('content')

    <div class="page-heading">
        <div class="page-title">
            <div class="row">
                <div class="col-12 col-md-6 order-md-1 order-last">
                    <h3 style="color: white !important">Nos Candidatures Spontanées</h3>
                    <p class="text-subtitle text-muted" style="color: white !important">La liste de tous nos candidats</p>
                </div>
                <div class="col-12 col-md-6 order-md-2 order-first">
                    <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                        <ol class="breadcrumb">
                            {{-- <a class="btn btn-warning float-right"href="{{ route('projects.create') }}">Ajouter une réalisation</a> --}}
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
        <section class="section">
            <div class="card">
                @include('spontaneous_applications.table')
            </div>
        </section>
    </div>

@endsection

