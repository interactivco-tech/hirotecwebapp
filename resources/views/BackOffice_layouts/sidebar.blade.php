<div class="sidebar-wrapper active">
    <div class="sidebar-header">
        <div class="d-flex justify-content-between">
            <div class="logo">
                <a href="{{ route('home') }}"><img src="{{asset('assets/images/LOGO HIROTEC.jpg')}}" alt="Logo" srcset=""></a>
            </div>
            <div class="toggler">
                <a href="#" class="sidebar-hide d-xl-none d-block"><i class="bi bi-x bi-middle"></i></a>
            </div>
        </div>
    </div>
    <div class="sidebar-menu">
        <ul class="menu">
            <li class="sidebar-item {{ Request::is('home*') ? 'active' : '' }}">
                <a href="{{ route('home') }}" class='sidebar-link'>
                    <i class="bi bi-grid-fill"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            <li class="sidebar-item {{ Request::is('projects*') ? 'active' : '' }}">
                <a href="{{ route('projects.index') }}" class='sidebar-link'>
                    <i class="bi bi-stack"></i>
                    <span>Nos Projets</span>
                </a>
            </li>
            <li class="sidebar-item {{ Request::is('categoryProjects*') ? 'active' : '' }}">
                <a href="{{ route('categoryProjects.index') }}" class='sidebar-link'>
                    <i class="bi bi-collection-fill"></i>
                    <span>Catégorie de Projets</span>
                </a>
            </li>
            {{-- <li class="sidebar-item {{ Request::is('trades*') ? 'active' : '' }}">
                <a href="{{ route('trades.index') }}" class='sidebar-link'>
                    <i class="bi bi-grid-1x2-fill"></i>
                    <span>Nos services</span>
                </a>
            </li> --}}
            <li class="sidebar-item {{ Request::is('testimonials*') ? 'active' : '' }}">
                <a href="{{ route('testimonials.index') }}" class='sidebar-link'>
                    <i class="bi bi-hexagon-fill"></i>
                    <span>Les Témoignages</span>
                </a>
            </li>
            {{-- <li class="sidebar-item {{ Request::is('domains*') ? 'active' : '' }}">
                <a href="{{ route('domains.index') }}" class='sidebar-link'>
                    <i class="bi bi-file-earmark-medical-fill"></i>
                    <span>Domaine d'Activité</span>
                </a>
            </li> --}}
            {{-- <li class="sidebar-item {{ Request::is('spontaneousApplications*') ? 'active' : '' }}">
                <a href="{{ route('spontaneousApplications.index') }}" class='sidebar-link'>
                    <i class="bi bi-pen-fill"></i>
                    <span>Les Candidatures</span>
                </a>
            </li> --}}
            <li class="sidebar-item {{ Request::is('customerRequests*') ? 'active' : '' }}">
                <a href="{{ route('customerRequests.index') }}" class='sidebar-link'>
                    <i class="bi bi-person-lines-fill"></i>
                    <span>Contacts Clients</span>
                </a>
            </li>
            <li class="sidebar-item {{ Request::is('actualities*') ? 'active' : '' }}">
                <a href="{{ route('actualities.index') }}" class='sidebar-link'>
                    <i class="bi bi-file-earmark-spreadsheet-fill"></i>
                    <span>Nos Actualités</span>
                </a>
            </li>
            <li class="sidebar-item {{ Request::is('teams*') ? 'active' : '' }}">
                <a href="{{ route('teams.index') }}" class='sidebar-link'>
                    <i class="bi bi-people-fill"></i>
                    <span>Notre Equipe</span>
                </a>
            </li>
            <li class="sidebar-item {{ Request::is('slides*') ? 'active' : '' }}">
                <a href="{{ route('slides.index') }}" class='sidebar-link'>
                    <i class="bi bi-file-slides-fill"></i>
                    <span>Gestion des Slides</span>
                </a>
            </li>
            <li class="sidebar-item {{ Request::is('newsletters*') ? 'active' : '' }}">
                <a href="{{ route('newsletters.index') }}" class='sidebar-link'>
                    <i class="bi bi-envelope-open-fill"></i>
                    <span>NewsLetters</span>
                </a>
            </li>
            <li class="sidebar-item {{ Request::is('users*') ? 'active' : '' }}">
                <a href="{{ route('users.index') }}" class='sidebar-link'>
                    <i class="bi bi-person-badge-fill"></i>
                    <span>Les Administrateurs</span>
                </a>
            </li>
        </ul>
    </div>
    <button class="sidebar-toggler btn x"><i data-feather="x"></i></button>
</div>
