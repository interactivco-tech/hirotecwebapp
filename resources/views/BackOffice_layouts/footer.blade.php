<footer>
    <div class="footer clearfix mb-0 text-muted">
        <div class="float-start">
            <p>2021 &copy; Hirotec</p>
        </div>
        <div class="float-end">
            <p>Crafted with <span class="text-danger"><i class="bi bi-heart-fill icon-mid"></i></span> by <a href="https://vanessalecosson.net/prestations-digitales/" target="blank">Interactivco</a></p>
        </div>
    </div>
</footer>
