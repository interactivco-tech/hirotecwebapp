<!-- Title Field -->
<div class="col-md-4 col-12">
    <div class="form-group">
        {!! Form::label('title', 'Titre du projet:') !!}
        {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Titre', 'onkeyup' => 'stringToSlug(this.value)']) !!}
    </div>
</div>

<!-- slug Field -->
<div class="col-md-4 col-12">
    <div class="form-group">
        {!! Form::label('slug', 'Slug:') !!}
        {!! Form::text('slug', null, ['class' => 'form-control', 'placeholder' => 'Slug', 'readonly']) !!}
    </div>
</div>

<!-- cateorie_projet_id Field -->
<div class="col-md-4 col-12">
    <div class="form-group">
        {!! Form::label('category_project_id', 'Catégorie du projet:') !!}
        <select class="form-select" id="category_project_id" name="category_project_id">
            @foreach($categoryProjects as $categoryProject)
                <option value="{{$categoryProject->id}}">{{$categoryProject->title}}</option>
            @endforeach
        </select>
    </div>
</div>


<!-- description Field -->
<div class="col-md-12">
    <div class="form-group">
        {!! Form::label('description', 'Description du projet:') !!}
        {!! Form::textarea('description', null, ['class' => 'form-control ckeditor', 'name' => 'description']) !!}
    </div>
</div>

<!-- location Field -->
<div class="col-md-6 col-12">
    <div class="form-group">
        {!! Form::label('location', 'Emplacement du Projet:') !!}
        {!! Form::text('location', null, ['class' => 'form-control', 'placeholder' => 'Emplacement du Projet']) !!}
    </div>
</div>

<!-- image Field -->
<div class="col-md-6 col-12">
    <div class="form-group">
        {!! Form::label('image', 'Image de garde:') !!}
        {!! Form::file('image', ['class' => 'form-control', 'id' => 'formFile']) !!}
    </div>
    <span>NB : Veuillez Télécharger une image de Taille 542*496</span>
</div>

<!-- description Field -->
<div class="col-md-12">
    <div class="form-group">
        {!! Form::label('sub_description', 'Sous-Description du projet:') !!}
        {!! Form::textarea('sub_description', null, ['class' => 'form-control ckeditor', 'name' => 'sub_description']) !!}
    </div>
</div>

<!-- picture_1 Field -->
<div class="col-md-6 col-12">
    <div class="form-group">
        {!! Form::label('picture_1', 'Image de Presentation:') !!}
        {!! Form::file('picture_1', ['class' => 'form-control', 'id' => 'formFile']) !!}
    </div>
    <span>NB : Veuillez Télécharger une image de Taille 1120*489</span>
</div>

<!-- picture_2 Field -->
<div class="col-md-6 col-12">
    <div class="form-group">
        {!! Form::label('picture_2', 'Image Second Texte:') !!}
        {!! Form::file('picture_2', ['class' => 'form-control', 'id' => 'formFile']) !!}
    </div>
    <span>NB : Veuillez Télécharger une image de Taille 560*335</span>
</div>

<!-- picture_3 Field -->
<div class="col-md-6 col-12">
    <div class="form-group">
        {!! Form::label('picture_3', 'Image du Bas:') !!}
        {!! Form::file('picture_3', ['class' => 'form-control', 'id' => 'formFile']) !!}
    </div>
    <span>NB : Veuillez Télécharger une image de Taille 1120*489</span>
</div>


<div class="col-12 d-flex justify-content-end mt-3">
    {!! Form::submit('Enregistré', ['class' => 'btn btn-warning me-1 mb-1']) !!}
    <a href="{{ route('projects.index') }}" class="btn btn-light-secondary me-1 mb-1">Annulé</a>
</div>

<script>
    function stringToSlug(str) {

        str = str.replace(/^\s+|\s+$/g, ''); // trim
        str = str.toLowerCase();

        // remove accents, swap ñ for n, etc
        var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
        var to = "aaaaeeeeiiiioooouuuunc------";
        for (var i = 0, l = from.length; i < l; i++) {
            str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
        }

        str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
            .replace(/\s+/g, '-') // collapse whitespace and replace by -
            .replace(/-+/g, '-'); // collapse dashes

        // return str;
        document.getElementById("slug").value = str;
    }

    function showPercentage(val){
        if(val == 1){
            document.getElementById("div_percentage").style.display= 'block';
        }else{
            document.getElementById("div_percentage").style.display= 'none';
        }
    }

    function showSold(val){
        if(val == 1){
            document.getElementById("div_sold").style.display= 'block';
        }else{
            document.getElementById("div_sold").style.display= 'none';
        }
    }
</script>
