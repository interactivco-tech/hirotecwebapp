<div class="row">
    <!-- Title At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('title', 'Titre du Projet:') !!}
            <p>{{ $project->title }}</p>
        </div>
    </div>
    <!-- Slug At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('slug', 'slug:') !!}
            <p>{{ $project->slug }}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <!-- Description At Field -->
    <div class="col-md-12 col-12">
        <div class="form-group">
            {!! Form::label('description', 'Description du Projet:') !!}
            <p>{!! $project->description !!}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <!-- Image At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('image', 'Image de Garde:') !!}
            <p><img src="{{ asset('/storage/images/' . $project->image) }}" alt="" width="250" height="200"></p>
        </div>
    </div>
    <!-- category_project_id At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('category_project_id', 'Catégorie du Projet:') !!}
            <p>{{ $project->getCategoriesProjectAttribute()->title ?? '---' }}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <!-- Description At Field -->
    <div class="col-md-12 col-12">
        <div class="form-group">
            {!! Form::label('sub_description', 'Sous-Description du Projet:') !!}
            <p>{!! $project->sub_description !!}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <!-- Image At Field -->
    <div class="col-md-4 col-12">
        <div class="form-group">
            {!! Form::label('picture_1', 'Image de Presentation:') !!}
            <p><img src="{{ asset('/storage/images/' . $project->picture_1) }}" alt="" width="250" height="200"></p>
        </div>
    </div>
    <!-- Image At Field -->
    <div class="col-md-4 col-12">
        <div class="form-group">
            {!! Form::label('picture_2', 'Image Second Texte:') !!}
            <p><img src="{{ asset('/storage/images/' . $project->picture_2) }}" alt="" width="250" height="200"></p>
        </div>
    </div>
    <!-- Image At Field -->
    <div class="col-md-4 col-12">
        <div class="form-group">
            {!! Form::label('picture_3', 'Image du Bas:') !!}
            <p><img src="{{ asset('/storage/images/' . $project->picture_3) }}" alt="" width="250" height="200"></p>
        </div>
    </div>
</div>


<div class="row mt-3">
    <!-- Created At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('created_at', 'Crée à:') !!}
            <p>{{ $project->created_at }}</p>
        </div>
    </div>
    <!-- Updated At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('updated_at', 'Modifié à:') !!}
            <p>{{ $project->updated_at }}</p>
        </div>
    </div>
</div>






