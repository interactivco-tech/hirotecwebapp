@extends('BackOffice_layouts.master_index')

@section('title')
    Nos Témoignages Clients  | {{ config('app.name') }}
@endsection

@section('content')

    <div class="page-heading">
        <div class="page-title">
            <div class="row">
                <div class="col-12 col-md-6 order-md-1 order-last">
                    <h3 style="color: white !important">Modifier le Témoignage</h3>
                    <p class="text-subtitle text-muted" style="color: white !important">Les informations sur le Témoignage du client</p>
                </div>
            </div>
        </div>

        <section id="basic-horizontal-layouts">
            <div class="row match-height">
                <div class="col-md-12 col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                {!! Form::model($testimonial, ['route' => ['testimonials.update', $testimonial->id], 'method' => 'patch', 'files' => true]) !!}
                                    <div class="row">
                                        @include('testimonials.fields')
                                    </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>

@endsection
