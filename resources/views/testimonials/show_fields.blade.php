
<div class="row mt-3">
    <!-- Customer_name At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('customer_name', 'Titre du Message:') !!}
            <p>{{ $testimonial->customer_name }}</p>
        </div>
    </div>
    <!-- Image At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('image', 'Image:') !!}
            <p><img src="{{ asset('/storage/images/' . $testimonial->image) }}" alt="" width="250" height="200"></p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <!-- Description At Field -->
    <div class="col-md-12 col-12">
        <div class="form-group">
            {!! Form::label('description', 'Message du Cient:') !!}
            <p>{!! $testimonial->description !!}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <!-- Created At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('created_at', 'Crée à:') !!}
            <p>{{ $testimonial->created_at }}</p>
        </div>
    </div>
    <!-- Updated At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('updated_at', 'Modifié à:') !!}
            <p>{{ $testimonial->updated_at }}</p>
        </div>
    </div>
</div>
