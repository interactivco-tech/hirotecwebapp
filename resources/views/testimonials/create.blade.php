{{-- @extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <h1>Create Testimonial</h1>
                </div>
            </div>
        </div>
    </section>

    <div class="content px-3">

        @include('adminlte-templates::common.errors')

        <div class="card">

            {!! Form::open(['route' => 'testimonials.store']) !!}

            <div class="card-body">

                <div class="row">
                    @include('testimonials.fields')
                </div>

            </div>

            <div class="card-footer">
                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                <a href="{{ route('testimonials.index') }}" class="btn btn-default">Cancel</a>
            </div>

            {!! Form::close() !!}

        </div>
    </div>
@endsection --}}

@extends('BackOffice_layouts.master_index')

@section('title')
    Nos Témoignages Clients | {{ config('app.name') }}
@endsection

@section('content')

<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-6 order-md-1 order-last">
                <h3 style="color: white !important">Créer un Nouveau Témoignage</h3>
                <p class="text-subtitle text-muted" style="color: white !important">Les informations sur le témoignage</p>
            </div>
        </div>
    </div>

    <section id="basic-horizontal-layouts">
        <div class="row match-height">
            <div class="col-md-12 col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            {!! Form::open(['route' => 'testimonials.store', 'class' => 'form', 'files' => true]) !!}
                                <div class="row">
                                    @include('testimonials.fields')
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</div>

@endsection
