{{-- <div class="table-responsive">
    <table class="table" id="newsletters-table">
        <thead>
            <tr>

                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($newsletters as $newsletter)
            <tr>

                <td width="120">
                    {!! Form::open(['route' => ['newsletters.destroy', $newsletter->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('newsletters.show', [$newsletter->id]) }}" class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('newsletters.edit', [$newsletter->id]) }}" class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div> --}}

<div class="card-body">
    <table class="table table-striped" id="table1">
        <thead>
            <tr>
                <th>Nom</th>
                <th>Email</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($newsletters as $newsletter)
                <tr>

                    <td>{{ $newsletter->name }}</td>
                    <td>{{ $newsletter->email }}</td>
                    <td width="120">
                        {!! Form::open(['route' => ['newsletters.destroy', $newsletter->id], 'method' => 'delete']) !!}
                            <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                <button type="button" class="btn btn-primary"><a href="{{ route('newsletters.show', [$newsletter->id]) }}" class='badge badge-pill badge-primary'>
                                    Voir
                                </a></button>
                                {{-- <button type="button" class="btn btn-warning"><a href="{{ route('newsletters.edit', [$newsletter->id]) }}" class='badge badge-pill badge-warning'>
                                    Editer
                                </a></button> --}}
                                {!! Form::button('Supprimer', ['type' => 'submit', 'class' => 'btn btn-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                            </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
