<li class="nav-item">
    <a href="{{ route('teams.index') }}"
       class="nav-link {{ Request::is('teams*') ? 'active' : '' }}">
        <p>Teams</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('testimonials.index') }}"
       class="nav-link {{ Request::is('testimonials*') ? 'active' : '' }}">
        <p>Testimonials</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('customerRequests.index') }}"
       class="nav-link {{ Request::is('customerRequests*') ? 'active' : '' }}">
        <p>Customer Requests</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('projects.index') }}"
       class="nav-link {{ Request::is('projects*') ? 'active' : '' }}">
        <p>Projects</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('categoryProjects.index') }}"
       class="nav-link {{ Request::is('categoryProjects*') ? 'active' : '' }}">
        <p>Category Projects</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('slides.index') }}"
       class="nav-link {{ Request::is('slides*') ? 'active' : '' }}">
        <p>Slides</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('spontaneousApplications.index') }}"
       class="nav-link {{ Request::is('spontaneousApplications*') ? 'active' : '' }}">
        <p>Spontaneous Applications</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('newsletters.index') }}"
       class="nav-link {{ Request::is('newsletters*') ? 'active' : '' }}">
        <p>Newsletters</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('domains.index') }}"
       class="nav-link {{ Request::is('domains*') ? 'active' : '' }}">
        <p>Domains</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('trades.index') }}"
       class="nav-link {{ Request::is('trades*') ? 'active' : '' }}">
        <p>Trades</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('actualities.index') }}"
       class="nav-link {{ Request::is('actualities*') ? 'active' : '' }}">
        <p>Actualities</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('users.index') }}"
       class="nav-link {{ Request::is('users*') ? 'active' : '' }}">
        <p>Users</p>
    </a>
</li>


