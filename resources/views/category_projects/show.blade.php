{{-- @extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Category Project Details</h1>
                </div>
                <div class="col-sm-6">
                    <a class="btn btn-default float-right"
                       href="{{ route('categoryProjects.index') }}">
                        Back
                    </a>
                </div>
            </div>
        </div>
    </section>

    <div class="content px-3">
        <div class="card">

            <div class="card-body">
                <div class="row">
                    @include('category_projects.show_fields')
                </div>
            </div>

        </div>
    </div>
@endsection --}}

@extends('BackOffice_layouts.master_index')

@section('title')
    Nos Catégories de Réalisation | {{ config('app.name') }}
@endsection

@section('content')

    <div class="page-heading">
        <div class="page-title">
            <div class="row">
                <div class="col-12 col-md-6 order-md-1 order-last">
                    <h3 style="color: white !important">Détails de la Catégorie</h3>
                </div>
                <div class="col-12 col-md-6 order-md-2 order-first">
                    <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                        <ol class="breadcrumb">
                            <a class="btn btn-warning float-right"href="{{ route('categoryProjects.index') }}">Retour</a>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>

        <div class="content px-3">
            <div class="card">
                <div class="card-body">
                    @include('category_projects.show_fields')
                </div>

            </div>
        </div>
    </div>
@endsection

