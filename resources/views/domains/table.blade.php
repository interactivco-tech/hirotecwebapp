<div class="card-body">
    <table class="table table-striped" id="table1">
        <thead>
            <tr>
                <th>Image</th>
                <th>Nom du domaine</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($domains as $domain)
                <tr>

                    <td><img src="{{ asset('/storage/images/' .$domain->image) }}" alt="{{ $domain->title }}" width="90"></td>
                    <td>{{ $domain->name }}</td>
                    <td width="120">
                        {!! Form::open(['route' => ['domains.destroy', $domain->id], 'method' => 'delete']) !!}
                            <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                <button type="button" class="btn btn-primary"><a href="{{ route('domains.show', [$domain->id]) }}" class='badge badge-pill badge-primary'>
                                    Voir
                                </a></button>
                                <button type="button" class="btn btn-warning"><a href="{{ route('domains.edit', [$domain->id]) }}" class='badge badge-pill badge-warning'>
                                    Editer
                                </a></button>
                                {!! Form::button('Supprimer', ['type' => 'submit', 'class' => 'btn btn-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                            </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
