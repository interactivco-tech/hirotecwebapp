<div class="row">
    <!-- Name At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('name', 'Nom du Domaine:') !!}
            <p>{{ $domain->name }}</p>
        </div>
    </div>
    <!-- Image At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('image', 'Image:') !!}
            <p><img src="{{ asset('/storage/images/' . $domain->image) }}" alt="" width="250" height="200"></p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <!-- Description At Field -->
    <div class="col-md-12 col-12">
        <div class="form-group">
            {!! Form::label('description', 'Description du Domaine:') !!}
            <p>{!! $domain->description !!}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <!-- Medias At Field -->
    <div class="col-md-12 col-12">
        <div class="form-group">
            {!! Form::label('medias', 'Autres images:') !!}
            <p>
                @if ($domain->file == null)
                    @php
                    $files = explode(',',trim(stripslashes($domain->medias),"[]"));
                    @endphp
                    @foreach ($files as $item)
                        @php
                        $item = trim($item, '"');
                        $url = "/storage/$item";
                        $arr = explode('/', $url);
                        $last = end($arr);
                        $download_link = "/download/$last";
                        @endphp
                        <a href="{{ url($download_link) }}">
                            <img src="{{ asset($url) }}" width="25%"/>
                        </a>
                    @endforeach
                @else
                    Aucune image de disponible
                @endif
            </p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <!-- Created At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('created_at', 'Crée à:') !!}
            <p>{{ $domain->created_at }}</p>
        </div>
    </div>
    <!-- Updated At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('updated_at', 'Modifié à:') !!}
            <p>{{ $domain->updated_at }}</p>
        </div>
    </div>
</div>
