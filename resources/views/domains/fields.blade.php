<!-- Title Field -->
<div class="col-md-6 col-12">
    <div class="form-group">
        {!! Form::label('name', 'Nom du domaine d&rsquo;activité:') !!}
        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nom du domaine', 'onkeyup' => 'stringToSlug(this.value)']) !!}
    </div>
</div>

<!-- image Field -->
<div class="col-md-6 col-12">
    <div class="form-group">
        {!! Form::label('image', 'Image:') !!}
        {!! Form::file('image', ['class' => 'form-control', 'id' => 'formFile']) !!}
    </div>
</div>

<!-- description Field -->
<div class="col-md-12">
    <div class="form-group">
        {!! Form::label('description', 'Description du domaine:') !!}
        {!! Form::textarea('description', null, ['class' => 'form-control ckeditor', 'name' => 'description']) !!}
    </div>
</div>

<!-- medias Field -->
<div class="col-md-6 col-12">
    <div class="form-group">
        {!! Form::label('medias', 'Images du domaine:') !!}
        {!! Form::file('medias[]', ['class' => 'form-control', 'id' => 'formFileMultiple', 'multiple' => true, 'accept' => 'image/*']) !!}
    </div>
</div>

<div class="col-12 d-flex justify-content-end mt-3">
    {!! Form::submit('Enregistré', ['class' => 'btn btn-warning me-1 mb-1']) !!}
    <a href="{{ route('domains.index') }}" class="btn btn-light-secondary me-1 mb-1">Annulé</a>
</div>
