<div class="row">
    <!-- Title At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('title', 'Titre du Slide:') !!}
            <p>{{ $slide->title }}</p>
        </div>
    </div>
    <!-- Subtitle At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('subtitle', 'Sous-Titre:') !!}
            <p>{{ $slide->subtitle }}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <!-- Image At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('image', 'Image:') !!}
            <p><img src="{{ asset('/storage/images/' . $slide->image) }}" alt="" width="250" height="200"></p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <!-- Created At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('created_at', 'Crée à:') !!}
            <p>{{ $slide->created_at }}</p>
        </div>
    </div>
    <!-- Updated At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('updated_at', 'Modifié à:') !!}
            <p>{{ $slide->updated_at }}</p>
        </div>
    </div>
</div>
