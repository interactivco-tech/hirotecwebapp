<!-- Title Field -->
<div class="col-md-6 col-12">
    <div class="form-group">
        {!! Form::label('title', 'Titre:') !!}
        {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Titre']) !!}
    </div>
</div>

<!-- SubTitle Field -->
<div class="col-md-6 col-12">
    <div class="form-group">
        {!! Form::label('subtitle', 'Sous-Titre:') !!}
        {!! Form::text('subtitle', null, ['class' => 'form-control', 'placeholder' => 'Sous-Titre']) !!}
    </div>
</div>

<!-- image Field -->
<div class="col-md-6 col-12">
    <div class="form-group">
        {!! Form::label('image', 'Image de garde:') !!}
        {!! Form::file('image', ['class' => 'form-control', 'id' => 'formFile']) !!}
        <span>NB : Veuillez Télécharger une image de Taille 1926*873</span>
    </div>
</div>


<div class="col-12 d-flex justify-content-end mt-3">
    {!! Form::submit('Enregistré', ['class' => 'btn btn-warning me-1 mb-1']) !!}
    <a href="{{ route('slides.index') }}" class="btn btn-light-secondary me-1 mb-1">Annulé</a>
</div>
