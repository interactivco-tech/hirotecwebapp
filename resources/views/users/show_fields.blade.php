<div class="row">
    <!-- Title At Field -->
    <div class="col-md-4 col-12">
        <div class="form-group">
            {!! Form::label('name', 'Nom Administrateur:') !!}
            <p>{{ $user->name }}</p>
        </div>
    </div>
    <!-- Slug At Field -->
    <div class="col-md-4 col-12">
        <div class="form-group">
            {!! Form::label('email', 'Email Administrateur:') !!}
            <p>{{ $user->email }}</p>
        </div>
    </div>
    <!-- Slug At Field -->
    <div class="col-md-4 col-12">
        <div class="form-group">
            {!! Form::label('password', 'Mot de Passe Administrateur:') !!}
            <p>{{ $user->password }}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <!-- Created At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('created_at', 'Crée à:') !!}
            <p>{{ $user->created_at }}</p>
        </div>
    </div>
    <!-- Updated At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('updated_at', 'Modifié à:') !!}
            <p>{{ $user->updated_at }}</p>
        </div>
    </div>
</div>
