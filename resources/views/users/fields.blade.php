<!-- Name Field -->
<div class="col-md-4 col-12">
    <div class="form-group">
        {!! Form::label('name', 'Nom:') !!}
        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nom Administrateur']) !!}
    </div>
</div>

<!-- Email Field -->
<div class="col-md-4 col-12">
    <div class="form-group">
        {!! Form::label('email', 'Email:') !!}
        {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Email Administrateur']) !!}
    </div>
</div>

<!-- Password Field -->
<div class="col-md-4 col-12">
    <div class="form-group">
        {!! Form::label('password', 'Mot de Passe:') !!}
        {!! Form::text('password', null, ['class' => 'form-control', 'placeholder' => 'Password Administrateur', 'id' => 'Password']) !!}
    </div>
</div>


<div class="col-12 d-flex justify-content-end mt-3">
    {!! Form::submit('Enregistré', ['class' => 'btn btn-warning me-1 mb-1']) !!}
    <a href="{{ route('users.index') }}" class="btn btn-light-secondary me-1 mb-1">Annulé</a>
</div>
