<div class="row">
    <!-- Title At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('title', 'Titre de l&rsquo;Actualité:') !!}
            <p>{{ $actuality->title }}</p>
        </div>
    </div>
    <!-- Slug At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('slug', 'Slug:') !!}
            <p>{{ $actuality->slug }}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <!-- Image At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('image', 'Image:') !!}
            <p><img src="{{ asset('/storage/images/' . $actuality->image) }}" alt="" width="250" height="200"></p>
        </div>
    </div>
    <!-- Actuality_date At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('actuality_date', 'Date de l&rsquo;Actualité:') !!}
            <p>{{ $actuality->actuality_date }}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <!-- Description At Field -->
    <div class="col-md-12 col-12">
        <div class="form-group">
            {!! Form::label('description', 'Description de l&rsquo;Actualité:') !!}
            <p>{!! $actuality->description !!}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <!-- Medias At Field -->
    <div class="col-md-12 col-12">
        <div class="form-group">
            {!! Form::label('medias', 'Autres images:') !!}
            <p>
                @if ($actuality->file == null)
                    @php
                    $files = explode(',',trim(stripslashes($actuality->medias),"[]"));
                    @endphp
                    @foreach ($files as $item)
                        @php
                        $item = trim($item, '"');
                        $url = "/storage/$item";
                        $arr = explode('/', $url);
                        $last = end($arr);
                        $download_link = "/download/$last";
                        @endphp
                        <a href="{{ url($download_link) }}">
                            <img src="{{ asset($url) }}" width="25%"/>
                        </a>
                    @endforeach
                @else
                    Aucune image de disponible
                @endif
            </p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <!-- Created At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('created_at', 'Crée à:') !!}
            <p>{{ $actuality->created_at }}</p>
        </div>
    </div>
    <!-- Updated At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('updated_at', 'Modifié à:') !!}
            <p>{{ $actuality->updated_at }}</p>
        </div>
    </div>
</div>
