<div class="card-body">
    <table class="table table-striped" id="table1">
        <thead>
            <tr>
                <th>Image</th>
                <th>Titre</th>
                <th>Emplacement</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($actualities as $actuality)
                <tr>

                    <td><img src="{{ asset('/storage/images/' .$actuality->image) }}" alt="{{ $actuality->title }}" width="90"></td>
                    <td>{{ $actuality->title }}</td>
                    <td>{{ $actuality->location }}</td>
                    <td width="120">
                        {!! Form::open(['route' => ['actualities.destroy', $actuality->id], 'method' => 'delete']) !!}
                            <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                <button type="button" class="btn btn-primary"><a href="{{ route('actualities.show', [$actuality->id]) }}" class='badge badge-pill badge-primary'>
                                    Voir
                                </a></button>
                                <button type="button" class="btn btn-warning"><a href="{{ route('actualities.edit', [$actuality->id]) }}" class='badge badge-pill badge-warning'>
                                    Editer
                                </a></button>
                                {!! Form::button('Supprimer', ['type' => 'submit', 'class' => 'btn btn-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                            </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
