<div class="row">
    <!-- Name At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('name', 'Nom du Client:') !!}
            <p>{{ $customerRequest->name }}</p>
        </div>
    </div>
    <!-- Phone At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('phone', 'Contact du Client:') !!}
            <p>{{ $customerRequest->phone }}</p>
        </div>
    </div>
</div>

<div class="row">
    <!-- Email At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('email', 'Email du Client:') !!}
            <p>{{ $customerRequest->email }}</p>
        </div>
    </div>
    <!-- Subject At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('subject', 'Objet du Message:') !!}
            <p>{{ $customerRequest->subject }}</p>
        </div>
    </div>
</div>

<div class="row">
    <!-- message At Field -->
    <div class="col-md-12 col-12">
        <div class="form-group">
            {!! Form::label('message', 'Message du client:') !!}
            <p>{!! $customerRequest->message !!}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <!-- Created At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('created_at', 'Soumis à:') !!}
            <p>{{ $customerRequest->created_at }}</p>
        </div>
    </div>
    <!-- Updated At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('updated_at', 'Modifié à:') !!}
            <p>{{ $customerRequest->updated_at }}</p>
        </div>
    </div>
</div>
