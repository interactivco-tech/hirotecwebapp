<div class="card-body">
    <table class="table table-striped" id="table1">
        <thead>
            <tr>
                <th>Nom du client</th>
                <th>Contact Téléphonique</th>
                <th>Email</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($customerRequests as $customerRequest)
                <tr>

                    <td>{{ $customerRequest->name }}</td>
                    <td>{{ $customerRequest->phone }}</td>
                    <td>{{ $customerRequest->email }}</td>

                    <td width="120">
                        {!! Form::open(['route' => ['customerRequests.destroy', $customerRequest->id], 'method' => 'delete']) !!}
                        <div class="btn-group mb-2" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-primary"><a
                                    href="{{ route('customerRequests.show', [$customerRequest->id]) }}"
                                    class='badge badge-pill badge-primary'>
                                    Voir
                                </a></button>
                            {!! Form::button('Supprimer', ['type' => 'submit', 'class' => 'btn btn-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
