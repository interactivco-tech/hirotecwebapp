<div class="row">
    <!-- Name At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('name', 'Nom de l&rsquo;Employé:') !!}
            <p>{{ $team->name }}</p>
        </div>
    </div>
    <!-- Post At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('post', 'Poste de l&rsquo;Employé:') !!}
            <p>{{ $team->post }}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <!-- Description At Field -->
    <div class="col-md-12 col-12">
        <div class="form-group">
            {!! Form::label('description', 'Message l&rsquo;Employé:') !!}
            <p>{!! $team->description !!}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <!-- Image At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('image', 'Image:') !!}
            <p><img src="{{ asset('/storage/images/' . $team->image) }}" alt="" width="250" height="200"></p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <!-- Created At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('created_at', 'Crée à:') !!}
            <p>{{ $team->created_at }}</p>
        </div>
    </div>
    <!-- Updated At Field -->
    <div class="col-md-6 col-12">
        <div class="form-group">
            {!! Form::label('updated_at', 'Modifié à:') !!}
            <p>{{ $team->updated_at }}</p>
        </div>
    </div>
</div>
