@extends('customerFrontOffice.frontLayouts.masterPage')

@section('title')
    Notre Savoir-Faire | {{ config('app.name') }}
@endsection

@section('content')

    <div class="sec-title-page">
        <div class="container title-page-header">
            <h1>NOTRE SAVOIR-FAIRE</h1>
            <span>Accueil - Notre savoir-faire</span>
        </div>
    </div>
    <div>
        <div class="container content">
             <!-- Back to top button -->
            <a id="button"></a>
            <div class="title-page-content">
                <h2>Domaines d'expertises</h2>
                <h5>La construction, l'aménagement et le design n'ont aucun secret pour nous.</h5>
            </div>
            <div class="content-savoir-faire">
                <div class="owl-carousel owl-theme">
                    <div class="item">
                        <h4>Bâtiment</h4>
                        <p>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                            when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                            It has survived not only five centuries, but also the leap into electronic typesetting, remaining
                            essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing
                            Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker incl
                        </p>
                        <div class="sect-img">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="first-bloc-img">
                                                <img src="{{asset('customerAsset/images/savoirFaire/steel-bar-on-site-of-construction.png')}}" alt="" srcset="">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="second-bloc-img">
                                                <div class="img---">
                                                    <img src="{{asset('customerAsset/images/savoirFaire/silhouette-construction_1127-3246.png')}}" alt="" srcset="">
                                                </div>
                                                <div class="img---">
                                                    <img src="{{asset('customerAsset/images/savoirFaire/armature-metallique-chantier-construction-plan-flou_220838-118.png')}}"
                                                        alt="" srcset="">
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                        </div>
                    </div>
                    <div class="item">
                        <h4>Bâtiment</h4>
                        <p>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                            when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                            It has survived not only five centuries, but also the leap into electronic typesetting, remaining
                            essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing
                            Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker incl
                        </p>
                        <div class="sect-img">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="first-bloc-img">
                                                <img src="{{asset('customerAsset/images/savoirFaire/steel-bar-on-site-of-construction.png')}}" alt="" srcset="">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="second-bloc-img">
                                                <div class="img---">
                                                    <img src="{{asset('customerAsset/images/savoirFaire/silhouette-construction_1127-3246.png')}}" alt="" srcset="">
                                                </div>
                                                <div class="img---">
                                                    <img src="{{asset('customerAsset/images/savoirFaire/armature-metallique-chantier-construction-plan-flou_220838-118.png')}}"
                                                        alt="" srcset="">
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                        </div>
                    </div>
                </div>
                <div class="title-page-content">
                    <h2>CHAMPS D'ACTION HIROTEC</h2>
                    <h5>Nous vous accompagnons à chaque étape de votre projet, allant de sa
                        conception à l'exécution de votre projet.</h5>
                </div>

                <div class="services-content">
                    <div class="swiper-container">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <h4>La conception</h4>
                                <p>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                    Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                                    when an unknown printer took a galley of type and scrambled it to make a type specimen
                                    book.
                                    It has survived not only five centuries, but also the leap into electronic typesetting,
                                    remaining essentially unchanged. It was popularised in the 1960s with the release of
                                    Letraset
                                    sheets containing Lorem Ipsum passages, and more recently with desktop publishing
                                    software like Aldus PageMaker incl
                                </p>
                            </div>
                            <div class="swiper-slide">
                                <h4>l’étude</h4>
                                <p>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                    Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                                    when an unknown printer took a galley of type and scrambled it to make a type specimen
                                    book.
                                    It has survived not only five centuries, but also the leap into electronic typesetting,
                                    remaining essentially unchanged. It was popularised in the 1960s with the release of
                                    Letraset
                                    sheets containing Lorem Ipsum passages, and more recently with desktop publishing
                                    software like Aldus PageMaker incl
                                </p>
                            </div>
                            <div class="swiper-slide">
                                <h4> le montage financier</h4>
                                <p>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                    Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                                    when an unknown printer took a galley of type and scrambled it to make a type specimen
                                    book.
                                    It has survived not only five centuries, but also the leap into electronic typesetting,
                                    remaining essentially unchanged. It was popularised in the 1960s with the release of
                                    Letraset
                                    sheets containing Lorem Ipsum passages, and more recently with desktop publishing
                                    software like Aldus PageMaker incl
                                </p>
                            </div>
                            <div class="swiper-slide">
                                <h4> l’exécution</h4>
                                <p>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                    Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                                    when an unknown printer took a galley of type and scrambled it to make a type specimen
                                    book.
                                    It has survived not only five centuries, but also the leap into electronic typesetting,
                                    remaining essentially unchanged. It was popularised in the 1960s with the release of
                                    Letraset
                                    sheets containing Lorem Ipsum passages, and more recently with desktop publishing
                                    software like Aldus PageMaker incl
                                </p>
                            </div>
                        </div>
                        <!-- Add Pagination -->
                        <div class="swiper-pagination"></div>
                    </div>

                </div>


            </div>
        </div>
    </div>
    <div class="section-herothec">
        <div class="build-family-project">
            <div class="container">
                <div class="row info-pr">
                    <div class="col-sm-6">
                        <div class="build">
                            <p>
                                Construisez votre projet <br />avec nous
                            </p>
                            <div class="hero--">
                                <div class="btn-- bt-herotech">
                                    <a href="{{route ('contact') }}">Prenons rendez-vous </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="herotech-family">
                            <p>
                                Rejoignez la famille <br /> HEROTECH !
                            </p>
                            <div class="hero--">
                                <div class="btn-- bt-herotech">
                                    <a href="{{route ('contact') }}">Prenons rendez-vous </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
