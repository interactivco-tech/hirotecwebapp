<div class="section-footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <div class="footer-logo">
                    <img src="{{asset('customerAsset/images/logo-footer.png')}}" alt="" srcset="">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="items-menu">
                    <ul>
                        <li><a href="{{url ('/') }}"><span class="num">01</span> ACCUEIL</a></li>
                        <li><a href="{{url ('apropos') }}"><span class="num">02</span> A PROPOS</a></li>
                        <li><a href="{{url ('savoir-faire') }}"><span class="num">03</span> SAVOIR-FAIRE</a></li>
                        <li><a href="{{route ('realisations') }}"><span class="num">04</span>REALISATIONS</a></li>
                        <li><a href="{{route ('actualites') }}"><span class="num">05</span>ACTUALITÉS</a></li>
                        <li><a href="{{route ('contact') }}"><span class="num">06</span>CONTACTS</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="newletters-footer">

                    <h3>ABONNEMENT A LA NEWSLETTER</h3>

                    <form action="{{ url('footer') }}" method="POST" id="form500">
                        @csrf
                            <input type="text" required class="form-control @error('name') is-invalid @enderror" id="name" placeholder="Nom & prénoms*" name="name" value="{{old('name')}}">

                            <input type="email"  required class="form-control @error('email') is-invalid @enderror" id="email" placeholder="Addresse E-mail*" name="email" value="{{old('email')}}">

                            <div class="btn-abonner">
                                <button type="submit" onclick="" value="Envoyer">S'ABONNER</button>
                            </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="footer-RS">
            <ul>
                <li><i class="fa fa-facebook" aria-hidden="true">&nbsp;</i></li>
                <li><i class="fa fa-linkedin" aria-hidden="true">&nbsp;</i></li>
                <li><i class="fa fa-instagram" aria-hidden="true">&nbsp;</i></li>
            </ul>
        </div>
        <div class="link-supp">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="first-link">
                            <ul>
                                <li><a href="#">Mentions légales</a></li>
                                <li><a href="#">Politique de confidentialité</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="second-link">
                            <p>Copyright HIROTEC 2021-Réalisé avec le cœur par<a href="https://vanessalecosson.net/prestations-digitales/" target="blank"> INTERACTIVCO </a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
