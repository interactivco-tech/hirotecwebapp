<!-- menu-->
<div id="header">
    <div class="container">
        <div class="logo">
            <a href="{{url ('/') }}"><img src="{{asset('customerAsset/images/logo-header.png')}}" alt="" srcset=""></a>
        </div>
        <div id="nav-toggle">
            <div>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>

        <div id="gloval-nav">
            <nav>
                <ul>
                    <li><a href="{{url ('/') }}">ACCUEIL</a></li>
                    <li><a href="{{route ('apropos') }}">A PROPOS</a></li>
                    <li><a href="{{route ('savoir-faire') }}">SAVOIR FAIRE</a></li>
                    <li><a href="{{route ('realisations') }}">RÉALISATIONS</a></li>
                    <li><a href="{{route ('actualites') }}">ACTUALITÉS</a></li>
                    <li><a href="{{route ('contact') }}">CONTACTS</a></li>
                </ul>
            </nav>
        </div><!-- /#gloval-nav -->
    </div>
</div>
<!-- menu-->
