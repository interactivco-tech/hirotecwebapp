<!DOCTYPE html>

<html lang="en">

    <head>

        <meta charset="UTF-8">

        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>@yield('title')</title>

        <link rel="icon" href="{{asset('assets/images/favicon.ico')}}" >

        <link rel="stylesheet" href="{{asset('customerAsset/css/index.css')}}">

        <!-- boostrap 4 -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <link rel="stylesheet" href="https://unpkg.com/swiper@7/swiper-bundle.min.css" />

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        <link rel="stylesheet" href="{{asset('customerAsset/css/swipper.css')}}">
        <link rel="stylesheet" href="{{asset('customerAsset/css/contact.css')}}">
        <link rel="stylesheet" href="{{asset('customerAsset/css/savoir-faire.css')}}">
        <link rel="stylesheet" href="{{asset('customerAsset/css/carousel-team.css')}}">
        <link rel="stylesheet" href="{{asset('customerAsset/css/actualite.css')}}">
        <link rel="stylesheet" href="{{asset('customerAsset/css/hamburgerMenu.css')}}">
        <link rel="stylesheet" href="{{asset('customerAsset/css/goto.css')}}">

        <!-- AOS ANIMATION -->
        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

        <!-- OWL caroussel css-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.css">

    </head>

    <body>

        @include('customerFrontOffice.frontLayouts.header')

        @yield('content')

        @include('customerFrontOffice.frontLayouts.footer')

        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" ></script>

        <!-- Swiper JS -->
        <script src="https://unpkg.com/swiper@7/swiper-bundle.min.js"></script>

        <script type="text/javascript">

            const swiper = new Swiper('.swiper-container', {

                direction: 'horizontal',
                slidesPerView: 1,
                loop: true,

                autoplay:true,

                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                },
            });

        </script>

        <!-- OWL carrousel -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>

        <script src="{{asset('customerAsset/js/hamburgerMenu.js')}}"></script>
        <script src="{{asset('customerAsset/js/slider-carousel.js')}}"></script>
        <script src="{{asset('customerAsset/js/carousel-teams.js')}}"></script>
        <script src="{{asset('customerAsset/js/GoTo.js')}}"></script>

        <!-- AOS ANIMATION -->
        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

    </body>

</html>
