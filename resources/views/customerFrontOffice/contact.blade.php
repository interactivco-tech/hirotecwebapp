@extends('customerFrontOffice.frontLayouts.masterPage')

@section('title')
    Nous-Contacter | {{ config('app.name') }}
@endsection

@section('content')

    <div class="sec-title-page">
        <div class="container title-page-header">
            <h1>CONTACTS </h1>
            <span>Accueil - Contacts</span>
        </div>
    </div>
    <div >
         <!-- Back to top button -->
         <a id="button"></a>
        <div class="container content">

            <div class="title-page-content">
                <h2>Nous-Contacter</h2>
                <p>Vous recherchez le partenaire idéal pour sortir vos projets de terre ?</p>
            </div>

            <div class="row infos">
                <div class="col-sm-6 forms-contact">
                    <form action="{{ url('contacter') }}" method="POST" id="form500">
                        @csrf
                            <fieldset>
                                <input type="text" required class="form-control @error('name') is-invalid @enderror" id="name"  placeholder="Nom & Prénom(s)" name="name" value="{{old('name')}}">

                                <input type="text" required class="form-control @error('phone') is-invalid @enderror" id="phone" placeholder="Votre Contact" name="phone" value="{{old('phone')}}">

                                <input type="email" required class="form-control @error('email') is-invalid @enderror" id="email" placeholder="Votre Email" name="email" value="{{old('email')}}">

                                <input type="text" required class="form-control @error('subject') is-invalid @enderror" name="subject" placeholder="Entrez l'objet" value="{{old('subject')}}">

                                <textarea type="text" required class="form-control @error('message') is-invalid @enderror" id="message" placeholder="Ecrivez votre message" name="message" cols="30" rows="10">{{old('message')}}</textarea>

                                <span>En cliquant sur "ENVOYER" je confirme avoir lu la politique de confidentialité</span>

                                <div class="btn-send">
                                    <button type="submit" onclick="" value="Envoyer">Envoyer</button>
                                </div>

                            </fieldset>
                    </form>
                </div>
                <div class="col-sm-6 coordonnes">

                    <h4>Coordonnés</h4>

                    <ul class="cont-mobile">
                        <li>Appelez-nous 24h/24</li>
                        <li>+225 00 00 00 00 00</li>
                    </ul>

                    <ul class="cont-mail">
                        <li>Enovez nous un E-mail</li>
                        <li>infos@hirotech.ci</li>
                    </ul>

                    <p>Rejoingnez la famille HIROTECH <br /> en soumettant votre <br /> candidature</p>
                    <div class="btn--">
                        <a href="#">Soumettre</a>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection
