@extends('customerFrontOffice.frontLayouts.masterPage')

@section('title')
    Nos Actualités | {{ config('app.name') }}
@endsection

@section('content')

    <div class="sec-title-page">
        <div class="container title-page-header">
            <h1>ACTUALITES </h1>
            <span>Accueil - Actualités</span>
        </div>
    </div>
    <div>
        <div class="container content">

            <div class="title-page-content">
                <h2>Actualités</h2>
                <p>Construire un avenir meilleur et radieux pour nos clients,
                    notre plus beau challenge ...</p>
            </div>
            <div class="content-actu">
                <div class="container">

                    <div class="row line-act">
                        @foreach ($actualities as $actualitie)
                            <div class="col-sm-6">
                                <div class="item-act">
                                    <div class="img-actualite">
                                        <img src="{{ asset('/storage/images/' . $actualitie->image) }}"
                                            alt="{{ $actualitie->title }}" srcset="">
                                    </div>
                                    <p class="tt-gr">{{ $actualitie->title }}</p>
                                    <div class="localisation">
                                        <span>LOCALISATION : {{ $actualitie->location }}</span>
                                    </div>
                                    <div class="link-actu">
                                        <a href="/detailactualites/{{$actualitie->slug}}">Voir l'actualité</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>

                </div>
            </div>

            <div class="nav-pagin mt-3">
                <ul class="pagination">
                    {!! $actualities->links() !!}
                </ul>
            </div>
        </div>
    </div>
    <div class="section-herothec">
        <div class="build-family-project">
            <div class="container">
                <div class="row info-pr">
                    <div class="col-sm-6">
                        <div class="build">
                            <p>
                                Construisez votre projet <br />avec nous
                            </p>
                            <div class="hero--">
                                <div class="btn-- bt-herotech">
                                    <a href="{{ route('contact') }}">Prenons rendez-vous </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="herotech-family">
                            <p>
                                Rejoignez la famille <br /> HIROTECH !
                            </p>
                            <div class="hero--">
                                <div class="btn-- bt-herotech">
                                    <a href="{{ route('contact') }}">Prenons rendez-vous </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
