@extends('customerFrontOffice.frontLayouts.masterPage')

@section('title')
    A propos | {{ config('app.name') }}
@endsection

@section('content')

    <div class="sec-title-page">
        <div class="container title-page-header">
            <h1>A PROPOS </h1>
            <span>Accueil - A propos</span>
        </div>
    </div>
    <div class="sec ">
         <!-- Back to top button -->
         <a id="button"></a>
        <div class="container content">

            <div class="title-page-content--">
                <h2>Notre histoire</h2>
            </div>
            <div class="content-a-propos">

                <div class="history">
                    <div class="row">
                            <h5>
                                HIROTEC, Lorem Ipsum is simply dummy text of the printing and typesetting indust
                            </h5>
                        <div class="col-sm-6">
                            <div class="title-with-img">
                                <div class="img-hist">
                                    <img src="{{asset('customerAsset/images/apropos/apropos-img-1.png')}}" alt="" srcset="">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="txt-history">
                                <p>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                                    has been the industry's standard dummy text ever since the 1500s, when an unknown
                                    printer took a galley of type and scrambled it to make a type specimen book. It has
                                    survived not only five centuries, but also the leap into electronic typesetting,
                                    remaining essentially unchanged. It was popularised in the 1960s with the release of
                                    Letraset sheets containing Lorem Ipsum passages,
                                    and more recently with desktop publishing software like Aldus PageMaker incl, Lorem
                                    Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                                    been the industry's standard dummy text ever since
                                    the 1500s, when an unknown printer took a galley of type and scrambled it to make a type
                                    specimen book. It has survived not only five centuries, but also the leap into
                                    electronic typesetting, remaining essentially unchanged.
                                    It was popularised in the 1960s with the release of Letraset sheets containing Lorem
                                    Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker
                                    incl
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="our-mission">
                    <div class="missions-V">
                        <h2>Nos missions et valeurs </h2>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="txt-valeur">
                                <div class="content-valeur">
                                    <div class="sec sect-v-1">
                                        <h4>Sécurité</h4>
                                        <p>
                                            Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                            Lorem Ipsum has been the industry's standard dummy text ever since the
                                            1500s, when an unknown printer took a galley of type and scrambled it to
                                            make a type specimen book.

                                        </p>
                                    </div>
                                    <div class="sec sect-v-1 protection">
                                        <h4>Organisation</h4>
                                        <p>
                                            Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                            Lorem Ipsum has been the industry's standard dummy text ever since the
                                            1500s, when an unknown printer took a galley of type and scrambled it to
                                            make a type specimen book.

                                        </p>
                                    </div>
                                    <div class="sec sect-v-1">
                                        <h4>Qualité</h4>
                                        <p>
                                            Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                            Lorem Ipsum has been the industry's standard dummy text ever since the
                                            1500s, when an unknown printer took a galley of type and scrambled it to
                                            make a type specimen book.

                                        </p>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="img-mission-v">
                                <img src="{{asset('customerAsset/images/apropos/a-propos-img-2.png')}}" alt="" srcset="">
                            </div>
                        </div>
                    </div>
                </div>
               <div class="our-strength">
                    <div class="row">
                            <div class="title-page-content---">
                                    <h2>Notre Force</h2>
                            </div>
                            <div class="title-with-strength-img">
                                <h5>
                                    Elle réside dans notre capcité à  Lorem Ipsum is simply dummy text of the printin
                                </h5>
                            </div>
                        <div class="col-sm-6">
                        <div class="title-with-strength-img">
                                <div class="img-strength">
                                    <img src="{{asset('customerAsset/images/apropos/a-propos-img-3.png')}}" alt="" srcset="">
                                </div>
                            </div>
                        </div> 
                        <div class="col-sm-6">
                            <div class="txt-strength">
                            <div class="content-valeur">
                                    <div class="sec sect-v-1">
                                        <h4>Politique qualité</h4>
                                        <p>
                                            Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                            Lorem Ipsum has been the industry's standard dummy text ever since the
                                            1500s, when an unknown printer took a galley of type and scrambled it to
                                            make a type specimen book.

                                        </p>
                                    </div>
                                    <div class="sec sect-v-1 protection">
                                        <h4>Politique HSE</h4>
                                        <p>
                                            Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                            Lorem Ipsum has been the industry's standard dummy text ever since the
                                            1500s, when an unknown printer took a galley of type and scrambled it to
                                            make a type specimen book.

                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div> 
            </div>
        </div>
        <div class="teams-chiffres">
            <div class="container">
                <div class="he-chiffres">
                    <h2>HIROTECH EN CHIFFRES</h2>
                    <img src="{{asset('customerAsset/images/apropos/h-chiffre.png')}}" alt="" srcset="">
                </div>
                <div class="teams">
                    <div class="container bloc-team">
                        <div class="title-page-content--">
                            <h2>L'Équipe dirigeante</h2>
                        </div>
                        <div class="txt-bloc-team">
                            <p>
                                HIROTEC est composée d'acteurs avec une très forte expérience <br /> ayant participés à la création
                                de ponts, d'ouvrages d'arts,<br /> de bâtiments résidentiels et commerciaux.
                            </p>
                        </div>
                        <div class="section-padding">
                            <div class="screenshot_slider owl-carousel">
                                @foreach ($teams as $team)
                                    <div class="item">
                                        <div class="t-1">
                                            <div class="team--">
                                                <div class="img-team">
                                                    <img src="{{ asset('/storage/images/' . $team->image) }}"
                                                    alt="{{ $team->name }}" srcset="">
                                                </div>
                                                <h4>{{ $team->name }}</h4>
                                            </div>
                                            <span class="description">
                                                <h4>{{ $team->name }}</h4>
                                                <div class="post">
                                                    {{ $team->post }}
                                                </div>
                                                <div class="txt-description">
                                                    <p>
                                                        {!! $team->description !!}
                                                    </p>

                                                </div>
                                            </span>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
    <div class="section-herothec">
        <div class="build-family-project">
            <div class="container">
                <div class="row info-pr">
                    <div class="col-sm-6">
                        <div class="build">
                            <p>
                                Construisez votre projet <br />avec nous
                            </p>
                            <div class="hero--">
                                <div class="btn-- bt-herotech">
                                    <a href="{{route ('contact') }}">Prenons rendez-vous </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="herotech-family">
                            <p>
                                Rejoignez la famille <br/> HIROTECH !
                            </p>
                            <div class="hero--">
                                <div class="btn-- bt-herotech">
                                    <a href="{{route ('contact') }}">Prenons rendez-vous </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
