@extends('customerFrontOffice.frontLayouts.master')

@section('title')
    Bienvenue | {{ config('app.name') }}
@endsection

@section('content')

    <div class="owl-carousel owl-theme slider">
        @foreach ($slides as $slide)

            <div class="item">
                <div class="slider-">
                    <img src="{{ asset('/storage/images/' . $slide->image) }}" alt="{{ $slide->title }}">
                    <div class="txt-slide">
                        <h1><span>{{ $slide->title }}</span> <br />
                            {{ $slide->subtitle }}</h1>
                    </div>
                </div>
            </div>

        @endforeach
    </div>
    <div class="sec">
        <!-- Back to top button -->
        <a id="button"></a>
        <div class="container content">
            <div class="content-index">
                <div class="expertise">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="expertise-img">
                                <img src="{{ asset('customerAsset/images/accueil/expertise.png') }}" alt="" srcset="">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="txt-expertise">
                                <div class="title-ex">
                                    <h4>Notre expertise</h4>
                                </div>
                                <p>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                    Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                                    when an unknown printer took a galley of type and scrambled it to make a type
                                    specimen book. It has survived not only five centuries, but also the leap into
                                    electronic typesetting, remaining essentially unchanged. It was popularised in
                                    the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,
                                    and more recently with desktop publishing software like Aldus PageMaker incl,
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                    Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                                    when an unknown printer took a galley of type and scrambled it to make a type
                                    specimen book. It has survived not only five centuries, but also the leap into
                                    electronic typesetting, remaining essentially unchanged. It was popularised in
                                    the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,
                                    and more recently with desktop publishing software like Aldus PageMaker incl
                                </p>
                                <div class="link-actu link-more">
                                    <a href="{{ route('apropos') }}">En savoir plus</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="our-devise">
                    <div class="title-page-content-- tt-">
                        <h2>Notre devise</h2>
                    </div>
                     <div class="swiper-container mySwiper">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <h4>Sécurité</h4>
                                <p>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                    Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                                    when an unknown printer took a galley of type and scrambled it to make a type specimen
                                    book.
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                    Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                                    when an unknown printer took a galley of type and scrambled it to make a type specimen
                                    book.

                                </p>
                            </div>
                            <div class="swiper-slide">
                                <h4>Protection</h4>
                                <p>
                                    It has survived not only five centuries, but also the leap into electronic typesetting,
                                    remaining essentially unchanged. It was popularised in the 1960s with the release of
                                    Letraset
                                    sheets containing Lorem Ipsum passages, and more
                                    It has survived not only five centuries, but also the leap into electronic typesetting,
                                    remaining essentially unchanged. It was popularised in the 1960s with the release of
                                    Letraset
                                    sheets containing Lorem Ipsum passages, and more
                                </p>

                            </div>
                            <div class="swiper-slide">
                                <h4>Sécurité</h4>
                                <p>
                                    It has survived not only five centuries, but also the leap into electronic typesetting,
                                    remaining essentially unchanged. It was popularised in the 1960s with the release of
                                    Letraset
                                    sheets containing Lorem Ipsum passages, and more
                                    It has survived not only five centuries, but also the leap into electronic typesetting,
                                    remaining essentially unchanged. It was popularised in the 1960s with the release of
                                    Letraset
                                    sheets containing Lorem Ipsum passages, and more
                                </p>
                            </div>
                        </div>
                         

                        <!-- Swiper -->
                        <div class="swiper-pagination"></div>
                    </div>

                </div>
            </div>

        </div>
        <div class="projects-index">
            <div class=" container txt-project">
                <div class="title-page-content-- tt-">
                    <h2>Les projets que nous réalisons</h2>
                </div>
                <p>Nous portons un regard neuf sur chacun de vos besoins afin d'apporter
                    une solution à vos projets de construction
                </p>
            </div>

            <div class="content-tabs">
                <div class="container tabs-item">
                    <ul class="nav nav-pills nav-fill navtop links-tabs">
                        @foreach ($categoryProjects as $categoryProject)
                            <li class="nav-item">
                                <a class="nav-link tf {{ $categoryProject->id == 1 ? 'active' : '' }}"
                                    href="#home{{ $categoryProject->id }}"
                                    data-toggle="tab">{{ $categoryProject->title }}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="tab-content">
                    @foreach ($categoryProjects as $categoryProject)
                        <div class="tab-pane {{ $categoryProject->id == 1 ? 'active' : '' }}" role="tabpanel"
                            id="home{{ $categoryProject->id }}">
                            <div class="tabs-projects">
                                <div class="row">
                                    @foreach ($categoryProject->projects as $project)

                                        @if ($loop->first)
                                            <div class="col-sm-6 img-col">
                                                <div class="img-projects">
                                                    <img src="{{ asset('/storage/images/' . $project->image) }}"
                                                        alt="{{ $project->title }}" srcset="">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="txt-projects">
                                                    <h3>{{ $project->title }}</h3>
                                                    <p>
                                                        {!! Str::limit($project->description, 700) !!}.
                                                    </p>

                                                    <div class="link-actu link-more project-link">
                                                        <a href="/detailrealisation/{{ $project->slug }}">Consulter le
                                                            projet</a>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>


        </div>
        <div class="team-et-heroChiffres">
            <div class="teams team-home">
                <div class="container bloc-team">
                    <div class="title-page-content--">
                        <h2>L'Équipe dirigeante</h2>
                    </div>
                    <div class="txt-bloc-team">
                        <p>
                            HIROTEC est composée d'acteurs avec une très forte expérience <br />
                            ayant participés à la création de ponts, d'ouvrages d'arts,<br />
                            de bâtiments résidentiels et commerciaux.
                        </p>
                    </div>
                    <div class="section-padding">
                        <div class="screenshot_slider owl-carousel">
                            @foreach ($teams as $team)

                                <div class="item">
                                    <div class="t-1">
                                        <div class="team--">
                                            <div class="img-team">
                                                <img src="{{ asset('/storage/images/' . $team->image) }}"
                                                    alt="{{ $team->name }}" srcset="">
                                            </div>
                                            <h4>{{ $team->name }}</h4>
                                        </div>
                                        <span class="description">
                                            <h4>{{ $team->name }}</h4>
                                            <div class="post">
                                                {{ $team->post }}
                                            </div>
                                            <div class="txt-description">
                                                <p>
                                                    {!! $team->description !!}
                                                </p>

                                            </div>
                                        </span>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>

                <div class="container hero-chiffre-home">
                    <h4>HIROTEC EN CHIFFRES</h4>
                    <div class="chiffre-H">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="dt-CH">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="chiffre--">
                                                <p>
                                                    38
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="txt-chiffre">
                                                <p>
                                                    Chantiers <br/>executés
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="dt-CH separator">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="chiffre--">
                                                <p>
                                                    09
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="txt-chiffre">
                                                <p>
                                                    Chantiers <br/>en Etude
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="dt-CH">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="chiffre--">
                                                    <p>
                                                        01
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="txt-chiffre">
                                                    <p>
                                                        Milliard <br/>de CA
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="container testimony">
            <div class="our-strength">
                <div class="title-page-content-- tt-">
                    <h2>Ce que disent nos clients</h2>
                </div>
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        @foreach ($testimonials as $testimonial)

                            <div class="swiper-slide">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="img-testi">
                                            <img src="{{ asset('/storage/images/' . $testimonial->image) }}"
                                                alt="{{ $testimonial->customer_name }}" srcset="">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="content-testimony">
                                            <h4>{{ $testimonial->customer_name }} ...</h4>
                                            <p>
                                                {!! Str::limit($testimonial->description, 300, '...') !!}

                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        @endforeach
                    </div>
                    <!-- Add Pagination -->
                    <div class="swiper-pagination"></div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="section-herothec">
        <div class="build-family-project">
            <div class="container">
                <div class="row info-pr">
                    <div class="col-sm-6">
                        <div class="build">
                            <p>
                                Construisez votre projet <br />avec nous
                            </p>
                            <div class="hero--">
                                <div class="btn-- bt-herotech">
                                    <a href="{{ route('contact') }}">Prenons rendez-vous </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="herotech-family">
                            <p>
                                Rejoignez la famille <br /> HIROTECH !
                            </p>
                            <div class="hero--">
                                <div class="btn-- bt-herotech">
                                    <a href="{{ route('contact') }}">Prenons rendez-vous </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
