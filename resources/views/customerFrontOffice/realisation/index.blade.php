@extends('customerFrontOffice.frontLayouts.masterPage')

@section('title')
    Nos Réalisations | {{ config('app.name') }}
@endsection

@section('content')

    <div class="sec-title-page">
        <div class="container title-page-header">
            <h1>NOS RÉALISATIONS</h1>
            <span>Accueil - Réalisations</span>
        </div>
    </div>


    <div class="">
        <div class="container content">
            <!-- Back to top button -->
            <a id="button"></a>
            <div class="title-page-content">
                <h2>Réalisations</h2>
                <p>Construire un avenir meilleur et radieux pour nos clients, notre plus beau challenge ...</p>
            </div>

            <div class="content-tabs">
                <div class="container">
                    <ul class="nav nav-pills nav-fill navtop">
                        @foreach ($categoryProjects as $categoryProject)
                            <li class="nav-item">
                                <a class="nav-link {{ $categoryProject->id == 1 ? 'active' : '' }}" role="tab"
                                    href="#home{{ $categoryProject->id }}"
                                    data-toggle="tab">{{ $categoryProject->title }}</a>
                            </li>
                        @endforeach
                    </ul>
                    <div class="tab-content">
                        @foreach ($categoryProjects as $categoryProject)
                            <div class="tab-pane {{ $categoryProject->id == 1 ? 'active' : '' }}" role="tabpanel"
                                id="home{{ $categoryProject->id }}" class="active">
                                <div class="content-actu">
                                    <div class="container">
                                        <div class="row line-act">
                                            @foreach ($categoryProject->projects as $project)
                                                <div class="col-sm-6">
                                                    <div class="item-rea">
                                                        <div class="img-realisation">
                                                            <img src="{{ asset('/storage/images/' . $project->image) }}"
                                                                alt="{{ $project->title }}" srcset="">
                                                        </div>
                                                        <p>{{ $project->title }}</p>
                                                        <div class="localisation">
                                                            <span>LOCALISATION : {{ $project->location }}</span>
                                                        </div>
                                                        <div class="link-projet">
                                                            <a href="/detailrealisation/{{ $project->slug }}">Voir le
                                                                projet</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>

                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section-herothec">
        <div class="build-family-project">
            <div class="container">
                <div class="row info-pr">
                    <div class="col-sm-6">
                        <div class="build">
                            <p>
                                Construisez votre projet <br />avec nous
                            </p>
                            <div class="hero--">
                                <div class="btn-- bt-herotech">
                                    <a href="{{ route('contact') }}">Prenons rendez-vous </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="herotech-family">
                            <p>
                                Rejoignez la famille <br /> HIROTECH !
                            </p>
                            <div class="hero--">
                                <div class="btn-- bt-herotech">
                                    <a href="{{ route('contact') }}">Prenons rendez-vous </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        jQuery(document).ready(function($) {
            $('select[title=categoryProjects]').on('change', function() {
                var selected = $(this).find(":selected").attr('value');
                $.ajax({
                    url: base_url + '/categoryProjects/' + selected + '/projects/',
                    type: 'GET',
                    dataType: 'json',

                }).done(function(data) {

                    var select = $('select[title=project]');
                    select.empty();
                    select.append('<option value="0" >Please Select Project</option>');
                    $.each(data, function(key, value) {
                        select.append('<option value=' + key.id + '>' + value.name +
                            '</option>');
                    });
                    console.log("success");
                })
            });
        });
    </script>

@endsection
