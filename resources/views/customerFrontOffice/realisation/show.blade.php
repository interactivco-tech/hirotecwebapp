@extends('customerFrontOffice.frontLayouts.masterPage')

@section('title')
    Détails Réalisations | {{ config('app.name') }}
@endsection

@section('content')

    <div class="sec-title-page">
        <div class="container title-page-header">
            <h1>{{ $project->title }}</h1>
            <span>Accueil - {{ $project->title }}</span>
        </div>
    </div>

    <div class="">
        <div class="container content">

            <div class="title-o-act">
                <h2>{{ $project->title }}</h2>
            </div>
            <div class="content-une-realisation">
                <p>
                    {!! $project->description !!}
                </p>

                <div class="large-img-realisation">
                    <img src="{{ asset('/storage/images/' . $project->picture_1) }}"
                    alt="{{ $project->title }}" srcset="">
                </div>

                <div class="row sec2">
                    <div class="col-sm-6">
                        <div class="txt-second-img">
                            <p>
                                {!! $project->sub_description !!}
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="second-mini-img">
                            <img src="{{ asset('/storage/images/' . $project->picture_2) }}"
                            alt="{{ $project->title }}" srcset="">
                        </div>
                    </div>
                </div>
                <div class="second-large-img">
                    <img src="{{ asset('/storage/images/' . $project->picture_3) }}"
                    alt="{{ $project->title }}" srcset="">
                </div>

                <div class="title-o-act other-title">
                    <h2>Autres Projets</h2>
                </div>

                <div class="row sec3">
                    @foreach ($projects as $project)
                    <div class="col-sm-6">
                        <div class="item-other-projects">
                            <div class="img-item-other">
                                <img src="{{ asset('/storage/images/' . $project->image) }}"
                                alt="{{ $project->title }}" srcset="">
                            </div>
                            <a href="/detailrealisation/{{$project->slug}}">{{ $project->title }}</a>
                        </div>
                    </div>
                    @endforeach
                </div>

            </div>
        </div>
    </div>
    <div class="section-herothec">
        <div class="build-family-project">
            <div class="container">
                <div class="row info-pr">
                    <div class="col-sm-6">
                        <div class="build">
                            <p>
                                Construisez votre projet <br />avec nous
                            </p>
                            <div class="hero--">
                                <div class="btn-- bt-herotech">
                                    <a href="{{route ('contact') }}">Prenons rendez-vous </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="herotech-family">
                            <p>
                                Rejoignez la famille <br /> HIROTECH !
                            </p>
                            <div class="hero--">
                                <div class="btn-- bt-herotech">
                                    <a href="{{route ('contact') }}">Prenons rendez-vous </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
