<div class="card-body">
    <table class="table table-striped" id="table1">
        <thead>
            <tr>
                <th>Nom du service</th>
                <th>Slug</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($trades as $trade)
                <tr>

                    {{-- <td><img src="{{ asset('/storage/images/' .$trade->image) }}" alt="{{ $trade->title }}" width="90"></td> --}}
                    <td>{{ $trade->title }}</td>
                    <td>{{ $trade->slug }}</td>
                    <td width="120">
                        {!! Form::open(['route' => ['trades.destroy', $trade->id], 'method' => 'delete']) !!}
                            <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                <button type="button" class="btn btn-primary"><a href="{{ route('trades.show', [$trade->id]) }}" class='badge badge-pill badge-primary'>
                                    Voir
                                </a></button>
                                <button type="button" class="btn btn-warning"><a href="{{ route('trades.edit', [$trade->id]) }}" class='badge badge-pill badge-warning'>
                                    Editer
                                </a></button>
                                {!! Form::button('Supprimer', ['type' => 'submit', 'class' => 'btn btn-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                            </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
